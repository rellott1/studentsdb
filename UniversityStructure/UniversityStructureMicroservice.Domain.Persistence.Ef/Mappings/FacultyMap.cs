﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UniversityStructureMicroservice.Domain.Faculties;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Mappings;

public class FacultyMap : IEntityTypeConfiguration<Faculty>
{
    public void Configure(EntityTypeBuilder<Faculty> builder)
    {
        builder.HasKey(e => e.FacultyId);
        builder.Property(e => e.Name)
            .HasMaxLength(255);
        
        builder.ToTable("T_Faculties");
    }
}