﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UniversityStructureMicroservice.Domain.Subjects;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Mappings;

public class SubjectGroupMap : IEntityTypeConfiguration<SubjectGroup>
{
    public void Configure(EntityTypeBuilder<SubjectGroup> builder)
    {
        builder.HasKey(e => e.Id);

        builder.HasOne(d => d.Subject)
            .WithMany(p => p.SubjectGroups)
            .HasForeignKey(d => d.SubjectId)
            .HasConstraintName("FK_T_SubjectGroup_T_Subject");

        builder.HasOne(d => d.Group)
            .WithMany(p => p.SubjectGroups)
            .HasForeignKey(d => d.GroupId)
            .HasConstraintName("FK_T_SubjectGroup_T_Group");

        builder.ToTable("T_SubjectGroups");
    }
}