﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UniversityStructureMicroservice.Domain.Departments;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Mappings;

public class DepartmentMap : IEntityTypeConfiguration<Department>
{
    public void Configure(EntityTypeBuilder<Department> builder)
    {
        builder.HasKey(e => e.DepartmentId);
        builder.Property(e => e.Name)
            .HasMaxLength(255);
        
        builder.HasOne(d => d.Faculty)
            .WithMany(p => p.Departments)
            .HasForeignKey(d => d.FacultyId)
            .HasConstraintName("FK_T_Department_T_Faculty");
        builder.ToTable("T_Departments");
    }
}