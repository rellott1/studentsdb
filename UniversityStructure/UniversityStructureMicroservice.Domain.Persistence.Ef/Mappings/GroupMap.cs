﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UniversityStructureMicroservice.Domain.Groups;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Mappings;

public class GroupMap : IEntityTypeConfiguration<Group>
{
    public void Configure(EntityTypeBuilder<Group> builder)
    {
        builder.HasKey(e => e.GroupId);
        
        builder.Property(e => e.Name)
            .HasMaxLength(255);
        
        builder.HasOne(d => d.Direction)
            .WithMany(p => p.Groups)
            .HasForeignKey(d => d.DirectionId)
            .HasConstraintName("FK_T_Group_T_Direction");
        
        builder.ToTable("T_Groups");
    }
}