﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UniversityStructureMicroservice.Domain.Directions;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Mappings;

public class DirectionMap : IEntityTypeConfiguration<Direction>
{
    public void Configure(EntityTypeBuilder<Direction> builder)
    {
        builder.HasKey(e => e.DirectionId);
        builder.Property(e => e.Name)
            .HasMaxLength(255);
        
        builder.HasOne(d => d.Faculty)
            .WithMany(p => p.Directions)
            .HasForeignKey(d => d.FacultyId)
            .HasConstraintName("FK_T_Direction_T_Faculty");
        builder.ToTable("T_Directions");
    }
}