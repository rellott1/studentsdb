﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UniversityStructureMicroservice.Domain.Subjects;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Mappings;

public class SubjectMap : IEntityTypeConfiguration<Subject>
{
    public void Configure(EntityTypeBuilder<Subject> builder)
    {
        builder.HasKey(e => e.SubjectId);
        
        builder.Property(e => e.Name)
            .HasMaxLength(255);

        builder.ToTable("T_Subjects");
    }
}