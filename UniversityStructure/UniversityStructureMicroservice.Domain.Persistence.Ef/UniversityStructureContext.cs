﻿using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Domain.Groups;
using UniversityStructureMicroservice.Domain.Subjects;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using UniversityStructureMicroservice.Domain.Persistence.Ef.Extensions;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef;

public class UniversityStructureContext : DbContext, IUniversityStructureContext
{
    public UniversityStructureContext(DbContextOptions<UniversityStructureContext> options) 
        : base(options)
    {
    }
    
    public DbSet<Department> Departments { get; set; }
    public DbSet<Direction> Directions { get; set; }
    public DbSet<Faculty> Faculties { get; set; }
    public DbSet<Group> Groups { get; set; }
    public DbSet<Subject> Subjects { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyEfConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}