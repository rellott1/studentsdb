﻿using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Domain.Groups;
using UniversityStructureMicroservice.Domain.Subjects;
using Microsoft.EntityFrameworkCore;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef;

public interface IUniversityStructureContext : IDisposable
{
    DbSet<Department> Departments { get; set; }
    DbSet<Direction> Directions { get; set; }
    DbSet<Faculty> Faculties { get; set; }
    DbSet<Group> Groups { get; set; }
    DbSet<Subject> Subjects { get; set; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}