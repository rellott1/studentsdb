﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Subjects;
using UniversityStructureMicroservice.Dto.Json.Subjects;
using Microsoft.EntityFrameworkCore;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;

public class SubjectRepository : EfRepository, ISubjectRepository
{
    public SubjectRepository(IUniversityStructureContext context) : base(context)
    {
    }

    public async Task<List<Subject>> GetSubjectsAsync()
    {
        return await Context.Subjects.ToListAsync();
    }

    public async Task<Subject> GetSubjectAsync(int subjectId)
    {
        return await Context.Subjects.FirstOrDefaultAsync(q => q.SubjectId == subjectId);
    }

    public async Task<Subject> CreateSubjectAsync(CreateSubjectDto createSubjectDto)
    {
        var subjectEntity = new Subject
        {
            Name = createSubjectDto.Name
        };
        await Context.Subjects.AddAsync(subjectEntity);
        await Context.SaveChangesAsync();
        return subjectEntity;
    }

    public async Task<Subject> UpdateSubjectAsync(int subjectId, JsonPatchDocument patch)
    {
        var subject = await Context.Subjects
            .SingleOrDefaultAsync(q => q.SubjectId == subjectId);

        if (subject == null)
            throw new Exception("Subject does not exist");
        
        patch.ApplyTo(subject);
        await Context.SaveChangesAsync();
        return subject;
    }

    public async Task<bool> RemoveSubjectAsync(int subjectId)
    {
        var subject = await Context.Subjects
            .SingleOrDefaultAsync(q => q.SubjectId == subjectId);

        if (subject == null)
        {
            // throw new Exception("Subject does not exist");
            return false;
        }

        Context.Subjects.Remove(subject);
        await Context.SaveChangesAsync();
        return true;
    }
}