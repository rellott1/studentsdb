﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Dto.Json.Departments;
using Microsoft.EntityFrameworkCore;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;

public class DepartmentRepository : EfRepository, IDepartmentRepository
{
    public DepartmentRepository(IUniversityStructureContext context) : base(context)
    {
    }

    public async Task<List<Department>> GetDepartmentsAsync()
    {
        return await Context.Departments.ToListAsync();
    }

    public async Task<Department> GetDepartmentAsync(int departmentId)
    {
        return await Context.Departments.FirstOrDefaultAsync(q => q.DepartmentId == departmentId);
    }

    public async Task<Department> CreateDepartmentAsync(CreateDepartmentDto createDepartmentDto)
    {
        var faculty = await Context.Faculties
            .FirstOrDefaultAsync(q => q.FacultyId == createDepartmentDto.FacultyId);

        if (faculty == null)
            return null;

        var departmentEntity = new Department
        {
            Name = createDepartmentDto.Name,
            FacultyId = createDepartmentDto.FacultyId
        };
        await Context.Departments.AddAsync(departmentEntity);
        await Context.SaveChangesAsync();
        return departmentEntity;
    }

    public async Task<Department> UpdateDepartmentAsync(int departmentId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/facultyId" && patch.Operations[0].op == "replace")
        {
            var faculty = await Context.Faculties
                .FirstOrDefaultAsync(q => q.FacultyId == (int) patch.Operations[0].value);

            if (faculty == null)
                return null;
        }
        
        var department = await Context.Departments
            .SingleOrDefaultAsync(q => q.DepartmentId == departmentId);

        if (department == null)
            throw new Exception("Department does not exist");
        
        patch.ApplyTo(department);
        await Context.SaveChangesAsync();
        return department;
    }

    public async Task<bool> RemoveDepartmentAsync(int departmentId)
    {
        var department = await Context.Departments
            .SingleOrDefaultAsync(q => q.DepartmentId == departmentId);

        if (department == null)
        {
            // throw new Exception("Faculty does not exist");
            return false;
        }

        Context.Departments.Remove(department);
        await Context.SaveChangesAsync();
        return true;
    }
}