﻿namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;

public class EfRepository
{
    protected readonly IUniversityStructureContext Context;

    protected EfRepository(IUniversityStructureContext context)
    {
        Context = context;
    }
}