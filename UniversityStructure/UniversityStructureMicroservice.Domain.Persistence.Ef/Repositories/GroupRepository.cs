﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Groups;
using UniversityStructureMicroservice.Dto.Json.Groups;
using Microsoft.EntityFrameworkCore;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;

public class GroupRepository : EfRepository, IGroupRepository
{
    public GroupRepository(IUniversityStructureContext context) : base(context)
    {
    }

    public async Task<List<Group>> GetGroupsAsync()
    {
        return await Context.Groups.ToListAsync();
    }

    public async Task<Group> GetGroupAsync(int groupId)
    {
        return await Context.Groups.FirstOrDefaultAsync(q => q.GroupId == groupId);
    }

    public async Task<Group> CreateGroupAsync(CreateGroupDto createGroupDto)
    {
        var direction = await Context.Directions
            .FirstOrDefaultAsync(q => q.DirectionId == createGroupDto.DirectionId);

        if (direction == null)
            return null;
        
        var groupEntity = new Group
        {
            Name = createGroupDto.Name,
            Course = (Course) createGroupDto.Course,
            EnrollmentYear = createGroupDto.EnrollmentYear,
            DirectionId = createGroupDto.DirectionId
        };

        await Context.Groups.AddAsync(groupEntity);
        await Context.SaveChangesAsync();
        return groupEntity;
    }

    public async Task<Group> UpdateGroupAsync(int groupId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/directionId" && patch.Operations[0].op == "replace")
        {
            var direction = await Context.Directions
                .FirstOrDefaultAsync(q => q.DirectionId == (int) patch.Operations[0].value);

            if (direction == null)
                return null;
        }
        
        var group = await Context.Groups
            .SingleOrDefaultAsync(q => q.GroupId == groupId);

        if (group == null)
            throw new Exception("Group does not exist");
        
        patch.ApplyTo(group);
        await Context.SaveChangesAsync();
        return group;
    }

    public async Task<bool> RemoveGroupAsync(int groupId)
    {
        var group = await Context.Groups
            .SingleOrDefaultAsync(q => q.GroupId == groupId);

        if (group == null)
        {
            // throw new Exception("Group does not exist");
            return false;
        }

        Context.Groups.Remove(group);
        await Context.SaveChangesAsync();
        return true;
    }
    
    public async Task<List<Group>> GetAllGroupsOfDirection(int directionId)
    {
        return await Context.Groups.Where(q => q.DirectionId == directionId).ToListAsync();
    }
}