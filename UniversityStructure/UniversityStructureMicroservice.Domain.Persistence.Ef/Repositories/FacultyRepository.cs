﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Dto.Json.Faculties;
using Microsoft.EntityFrameworkCore;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;

public class FacultyRepository : EfRepository, IFacultyRepository
{
    public FacultyRepository(IUniversityStructureContext context) : base(context)
    {
    }

    public async Task<List<Faculty>> GetFacultiesAsync()
    {
        return await Context.Faculties.ToListAsync();
    }

    public async Task<Faculty> GetFacultyAsync(int facultyId)
    {
        return await Context.Faculties.FirstOrDefaultAsync(q => q.FacultyId == facultyId);
    }

    public async Task<Faculty> CreateFacultyAsync(CreateFacultyDto createFacultyDto)
    {
        var facultyEntity = new Faculty
        {
            Name = createFacultyDto.Name
        };
        await Context.Faculties.AddAsync(facultyEntity);
        await Context.SaveChangesAsync();
        return facultyEntity;
    }

    public async Task<Faculty> UpdateFacultyAsync(int facultyId, JsonPatchDocument patch)
    {
        var faculty = await Context.Faculties
            .SingleOrDefaultAsync(q => q.FacultyId == facultyId);

        if (faculty == null)
            throw new Exception("Faculty does not exist");
        
        patch.ApplyTo(faculty);
        await Context.SaveChangesAsync();
        return faculty;
    }

    public async Task<bool> RemoveFacultyAsync(int facultyId)
    {
        var faculty = await Context.Faculties
            .SingleOrDefaultAsync(q => q.FacultyId == facultyId);

        if (faculty == null)
        {
            // throw new Exception("Faculty does not exist");
            return false;
        }

        Context.Faculties.Remove(faculty);
        await Context.SaveChangesAsync();
        return true;
    }
}