﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Dto.Json.Directions;
using Microsoft.EntityFrameworkCore;

namespace UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;

public class DirectionRepository : EfRepository, IDirectionRepository
{
    public DirectionRepository(IUniversityStructureContext context) : base(context)
    {
    }

    public async Task<List<Direction>> GetDirectionsAsync()
    {
        return await Context.Directions.ToListAsync();
    }

    public async Task<Direction> GetDirectionAsync(int directionId)
    {
        return await Context.Directions.FirstOrDefaultAsync(q => q.DirectionId == directionId);
    }

    public async Task<Direction> CreateDirectionAsync(CreateDirectionDto createDirectionDto)
    {
        var faculty = await Context.Faculties
            .FirstOrDefaultAsync(q => q.FacultyId == createDirectionDto.FacultyId);

        if (faculty == null)
            return null;
        
        var directionEntity = new Direction
        {
            Name = createDirectionDto.Name,
            FacultyId = createDirectionDto.FacultyId
        };
        await Context.Directions.AddAsync(directionEntity);
        await Context.SaveChangesAsync();
        return directionEntity;
    }

    public async Task<Direction> UpdateDirectionAsync(int directionId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/facultyId" && patch.Operations[0].op == "replace")
        {
            var faculty = await Context.Faculties
                .FirstOrDefaultAsync(q => q.FacultyId == (int) patch.Operations[0].value);

            if (faculty == null)
                return null;
        }
        
        var direction = await Context.Directions
            .SingleOrDefaultAsync(q => q.DirectionId == directionId);

        if (direction == null)
            throw new Exception("Direction does not exist");
        
        patch.ApplyTo(direction);
        await Context.SaveChangesAsync();
        return direction;
    }

    public async Task<bool> RemoveDirectionAsync(int directionId)
    {
        var direction = await Context.Directions
            .SingleOrDefaultAsync(q => q.DirectionId == directionId);

        if (direction == null)
        {
            // throw new Exception("Faculty does not exist");
            return false;
        }

        Context.Directions.Remove(direction);
        await Context.SaveChangesAsync();
        return true;
    }
    
    public async Task<List<Direction>> GetAllDirectionsOfFaculty(int facultyId)
    {
        return await Context.Directions.Where(q => q.FacultyId == facultyId).ToListAsync();
    }
}