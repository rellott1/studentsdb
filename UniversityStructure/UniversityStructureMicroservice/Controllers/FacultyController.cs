﻿using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Dto.Json.Faculties;

namespace UniversityStructureMicroservice.Controllers;

public class FacultyController
{
    private readonly IFacultyRepository _facultyRepository;

    public FacultyController(IFacultyRepository facultyRepository)
    {
        _facultyRepository = facultyRepository;
    }

    public async Task<List<Faculty>> GetFacultiesAsync()
    {
        return await _facultyRepository.GetFacultiesAsync();
    }

    public async Task<Faculty> GetFacultyAsync(int facultyId)
    {
        return await _facultyRepository.GetFacultyAsync(facultyId);
    }

    public async Task<Faculty> CreateFacultyAsync(CreateFacultyDto createFacultyDto)
    {
        return await _facultyRepository.CreateFacultyAsync(createFacultyDto);
    }

    public async Task<Faculty> UpdateFacultyAsync(int facultyId, JsonPatchDocument patch)
    {
        return await _facultyRepository.UpdateFacultyAsync(facultyId, patch);
    }

    public async Task<bool> RemoveFacultyAsync(int facultyId)
    {
        return await _facultyRepository.RemoveFacultyAsync(facultyId);
    }
}