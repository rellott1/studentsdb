﻿using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Dto.Json.Directions;

namespace UniversityStructureMicroservice.Controllers;

public class DirectionController
{
    private readonly IDirectionRepository _directionRepository;

    public DirectionController(IDirectionRepository directionRepository)
    {
        _directionRepository = directionRepository;
    }

    public async Task<List<Direction>> GetDirectionsAsync()
    {
        return await _directionRepository.GetDirectionsAsync();
    }

    public async Task<Direction> GetDirectionAsync(int directionId)
    {
        return await _directionRepository.GetDirectionAsync(directionId);
    }

    public async Task<Direction> CreateDirectionAsync(CreateDirectionDto createDirectionDto)
    {
        return await _directionRepository.CreateDirectionAsync(createDirectionDto);
    }

    public async Task<Direction> UpdateDirectionAsync(int directionId, JsonPatchDocument patch)
    {
        return await _directionRepository.UpdateDirectionAsync(directionId, patch);
    }

    public async Task<bool> RemoveDirectionAsync(int directionId)
    {
        return await _directionRepository.RemoveDirectionAsync(directionId);
    }
    
    public async Task<List<Direction>> GetAllDirectionsOfFaculty(int facultyId)
    {
        return await _directionRepository.GetAllDirectionsOfFaculty(facultyId);
    }
}