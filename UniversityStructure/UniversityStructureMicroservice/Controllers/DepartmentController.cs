﻿using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Dto.Json.Departments;

namespace UniversityStructureMicroservice.Controllers;

public class DepartmentController
{
    private readonly IDepartmentRepository _departmentRepository;

    public DepartmentController(IDepartmentRepository departmentRepository)
    {
        _departmentRepository = departmentRepository;
    }

    public async Task<List<Department>> GetDepartmentsAsync()
    {
        return await _departmentRepository.GetDepartmentsAsync();
    }

    public async Task<Department> GetDepartmentAsync(int departmentId)
    {
        return await _departmentRepository.GetDepartmentAsync(departmentId);
    }

    public async Task<Department> CreateDepartmentAsync(CreateDepartmentDto createDepartmentDto)
    {
        return await _departmentRepository.CreateDepartmentAsync(createDepartmentDto);
    }

    public async Task<Department> UpdateDepartmentAsync(int departmentId, JsonPatchDocument patch)
    {
        return await _departmentRepository.UpdateDepartmentAsync(departmentId, patch);
    }

    public async Task<bool> RemoveDepartmentAsync(int departmentId)
    {
        return await _departmentRepository.RemoveDepartmentAsync(departmentId);
    }
}