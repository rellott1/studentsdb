﻿using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Subjects;
using UniversityStructureMicroservice.Dto.Json.Subjects;

namespace UniversityStructureMicroservice.Controllers;

public class SubjectController
{
    private readonly ISubjectRepository _subjectRepository;

    public SubjectController(ISubjectRepository subjectRepository)
    {
        _subjectRepository = subjectRepository;
    }

    public async Task<List<Subject>> GetSubjectsAsync()
    {
        return await _subjectRepository.GetSubjectsAsync();
    }

    public async Task<Subject> GetSubjectAsync(int subjectId)
    {
        return await _subjectRepository.GetSubjectAsync(subjectId);
    }

    public async Task<Subject> CreateSubjectAsync(CreateSubjectDto createSubjectDto)
    {
        return await _subjectRepository.CreateSubjectAsync(createSubjectDto);
    }

    public async Task<Subject> UpdateSubjectAsync(int subjectId, JsonPatchDocument patch)
    {
        return await _subjectRepository.UpdateSubjectAsync(subjectId, patch);
    }

    public async Task<bool> RemoveSubjectAsync(int subjectId)
    {
        return await _subjectRepository.RemoveSubjectAsync(subjectId);
    }
}