﻿using Microsoft.AspNetCore.JsonPatch;
using UniversityStructureMicroservice.Domain.Groups;
using UniversityStructureMicroservice.Dto.Json.Groups;

namespace UniversityStructureMicroservice.Controllers;

public class GroupController
{
    private readonly IGroupRepository _groupRepository;

    public GroupController(IGroupRepository groupRepository)
    {
        _groupRepository = groupRepository;
    }

    public async Task<List<Group>> GetGroupsAsync()
    {
        return await _groupRepository.GetGroupsAsync();
    }

    public async Task<Group> GetGroupAsync(int groupId)
    {
        return await _groupRepository.GetGroupAsync(groupId);
    }

    public async Task<Group> CreateGroupAsync(CreateGroupDto createGroupDto)
    {
        return await _groupRepository.CreateGroupAsync(createGroupDto);
    }

    public async Task<Group> UpdateGroupAsync(int groupId, JsonPatchDocument patch)
    {
        return await _groupRepository.UpdateGroupAsync(groupId, patch);
    }

    public async Task<bool> RemoveGroupAsync(int groupId)
    {
        return await _groupRepository.RemoveGroupAsync(groupId);
    }

    public async Task<List<Group>> GetAllGroupsOfDirection(int directionId)
    {
        return await _groupRepository.GetAllGroupsOfDirection(directionId);
    }
}