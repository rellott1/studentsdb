﻿using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Domain.Subjects;

namespace UniversityStructureMicroservice.Domain.Groups;

public class Group
{
    public int GroupId { get; set; }
    public string Name { get; set; }
    public Course Course { get; set; }
    public DateTime EnrollmentYear { get; set; }
    public int DirectionId { get; set; }
    public virtual Direction Direction { get; set; }
    
    public ICollection<SubjectGroup> SubjectGroups { get; set; }
}