﻿namespace UniversityStructureMicroservice.Domain.Groups;

public enum Course : byte
{
    First = 1,
    Second = 2,
    Third = 3, 
    Fourth = 4,
    Fifth = 5,
    Sixth = 6
}