﻿using UniversityStructureMicroservice.Dto.Json.Groups;
using Microsoft.AspNetCore.JsonPatch;

namespace UniversityStructureMicroservice.Domain.Groups;

public interface IGroupRepository
{
    Task<List<Group>> GetGroupsAsync();
    Task<Group> GetGroupAsync(int groupId);
    Task<Group> CreateGroupAsync(CreateGroupDto createGroupDto);
    Task<Group> UpdateGroupAsync(int groupId, JsonPatchDocument patch);
    Task<bool> RemoveGroupAsync(int groupId);
    Task<List<Group>> GetAllGroupsOfDirection(int directionId);
}