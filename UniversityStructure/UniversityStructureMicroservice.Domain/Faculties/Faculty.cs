﻿using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Domain.Directions;

namespace UniversityStructureMicroservice.Domain.Faculties;

public class Faculty
{
    public int FacultyId { get; set; }
    public string Name { get; set; }
    public ICollection<Department> Departments { get; set; }
    public ICollection<Direction> Directions { get; set; }
}