﻿using UniversityStructureMicroservice.Dto.Json.Faculties;
using Microsoft.AspNetCore.JsonPatch;

namespace UniversityStructureMicroservice.Domain.Faculties;

public interface IFacultyRepository
{
    Task<List<Faculty>> GetFacultiesAsync();
    Task<Faculty> GetFacultyAsync(int facultyId);
    Task<Faculty> CreateFacultyAsync(CreateFacultyDto createFacultyDto);
    Task<Faculty> UpdateFacultyAsync(int facultyId, JsonPatchDocument patch);
    Task<bool> RemoveFacultyAsync(int facultyId);
}