﻿using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Domain.Groups;

namespace UniversityStructureMicroservice.Domain.Directions;

public class Direction
{
    public int DirectionId { get; set; }
    public string Name { get; set; }
    public int FacultyId { get; set; }
    public virtual Faculty Faculty { get; set; }
    public ICollection<Group> Groups { get; set; }
}