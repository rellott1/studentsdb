﻿using UniversityStructureMicroservice.Dto.Json.Directions;
using Microsoft.AspNetCore.JsonPatch;

namespace UniversityStructureMicroservice.Domain.Directions;

public interface IDirectionRepository
{
    Task<List<Direction>> GetDirectionsAsync();
    Task<Direction> GetDirectionAsync(int directionId);
    Task<Direction> CreateDirectionAsync(CreateDirectionDto createDirectionDto);
    Task<Direction> UpdateDirectionAsync(int directionId, JsonPatchDocument patch);
    Task<bool> RemoveDirectionAsync(int directionId);
    Task<List<Direction>> GetAllDirectionsOfFaculty(int facultyId);
}