﻿namespace UniversityStructureMicroservice.Domain.StudyHours;

public enum StudyHour : byte
{
    FirstSecond = 1,
    ThirdFourth = 2,
    FifthSixth = 3,
    SeventhEighth = 4,
    NinthTenth = 5,
    EleventhTwelfth = 6
}