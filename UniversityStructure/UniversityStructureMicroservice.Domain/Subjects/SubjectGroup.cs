﻿using UniversityStructureMicroservice.Domain.Groups;

namespace UniversityStructureMicroservice.Domain.Subjects;

public class SubjectGroup
{
    public int Id { get; set; }
    public int SubjectId { get; set; }
    public int GroupId { get; set; }
    
    public virtual Subject Subject { get; set; }
    public virtual Group Group { get; set; }
}