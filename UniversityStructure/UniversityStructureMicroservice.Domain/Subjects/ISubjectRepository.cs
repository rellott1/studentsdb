﻿using UniversityStructureMicroservice.Dto.Json.Subjects;
using Microsoft.AspNetCore.JsonPatch;

namespace UniversityStructureMicroservice.Domain.Subjects;

public interface ISubjectRepository
{
    Task<List<Subject>> GetSubjectsAsync();
    Task<Subject> GetSubjectAsync(int subjectId);
    Task<Subject> CreateSubjectAsync(CreateSubjectDto createSubjectDto);
    Task<Subject> UpdateSubjectAsync(int subjectId, JsonPatchDocument patch);
    Task<bool> RemoveSubjectAsync(int subjectId);
}