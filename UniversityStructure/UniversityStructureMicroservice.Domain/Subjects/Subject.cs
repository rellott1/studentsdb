﻿namespace UniversityStructureMicroservice.Domain.Subjects;

public class Subject
{
    public int SubjectId { get; set; }
    public string Name { get; set; }
    
    public ICollection<SubjectGroup> SubjectGroups { get; set; }
}