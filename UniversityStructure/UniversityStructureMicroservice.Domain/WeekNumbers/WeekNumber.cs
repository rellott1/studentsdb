﻿namespace UniversityStructureMicroservice.Domain.WeekNumbers;

public enum WeekNumber : byte
{
    Odd = 1,
    Even = 2
}