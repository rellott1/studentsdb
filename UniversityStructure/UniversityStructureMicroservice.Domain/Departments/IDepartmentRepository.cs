﻿using UniversityStructureMicroservice.Dto.Json.Departments;
using Microsoft.AspNetCore.JsonPatch;

namespace UniversityStructureMicroservice.Domain.Departments;

public interface IDepartmentRepository
{
    Task<List<Department>> GetDepartmentsAsync();
    Task<Department> GetDepartmentAsync(int departmentId);
    Task<Department> CreateDepartmentAsync(CreateDepartmentDto createDepartmentDto);
    Task<Department> UpdateDepartmentAsync(int departmentId, JsonPatchDocument patch);
    Task<bool> RemoveDepartmentAsync(int departmentId);
}