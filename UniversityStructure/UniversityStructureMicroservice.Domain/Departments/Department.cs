﻿using UniversityStructureMicroservice.Domain.Faculties;

namespace UniversityStructureMicroservice.Domain.Departments;

public class Department
{
    public int DepartmentId { get; set; }
    public string Name { get; set; }
    public int FacultyId { get; set; }
    public virtual Faculty Faculty { get; set; }
}