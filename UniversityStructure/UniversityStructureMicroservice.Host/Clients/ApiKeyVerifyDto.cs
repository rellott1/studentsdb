﻿namespace UniversityStructureMicroservice.Host.Clients;

public class ApiKeyVerifyDto
{
    public string UserName { get; set; }
    public Guid? UserGuid { get; set; }
    public bool IsValid { get; set; }
    public string ApiKey { get; set; }

    public ApiKeyVerifyDto()
    {

    }
}