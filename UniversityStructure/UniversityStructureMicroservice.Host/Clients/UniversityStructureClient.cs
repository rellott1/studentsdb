﻿using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace UniversityStructureMicroservice.Host.Clients;

public class UniversityStructureClient
{
    private readonly HttpClient _client;

    public UniversityStructureClient(HttpClient client)
    {
        _client = client;
    }
    
    public async Task<ApiKeyVerifyDto> VerifyApiKey(string apiKey)
    {
        var apiKeyVerifyDto = new ApiKeyVerifyDto();
        apiKeyVerifyDto.IsValid = false;
        apiKeyVerifyDto.ApiKey = apiKey;
        if (apiKey == "777")
        {
            apiKeyVerifyDto.IsValid = true;
            apiKeyVerifyDto.UserGuid = new Guid();
            apiKeyVerifyDto.UserName = "testAdm";
        }

        return apiKeyVerifyDto;
    }
}