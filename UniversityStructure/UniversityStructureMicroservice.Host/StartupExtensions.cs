﻿using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using UniversityStructureMicroservice.Controllers;
using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Domain.Groups;
using UniversityStructureMicroservice.Domain.Subjects;
using UniversityStructureMicroservice.Domain.Persistence.Ef;
using UniversityStructureMicroservice.Domain.Persistence.Ef.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: InternalsVisibleTo("UniversityStructureMicroservice.Host")]
namespace UniversityStructureMicroservice.Host;

public static class StartupExtensions
{
    public static IServiceCollection RegisterApplicationControllers(this IServiceCollection @this)
    {
        @this.AddScoped<FacultyController, FacultyController>();
        @this.AddScoped<DepartmentController, DepartmentController>();
        @this.AddScoped<DirectionController, DirectionController>();
        @this.AddScoped<GroupController, GroupController>();
        @this.AddScoped<SubjectController, SubjectController>();
        return @this;
    }
    
    public static IServiceCollection RegisterServices(this IServiceCollection @this)
    {
        return @this;
    }
    
    public static IServiceCollection RegisterDatabase(this IServiceCollection @this, string connectionString)
    {
        @this.AddDbContext<UniversityStructureContext>(options =>
            options.UseSqlServer(connectionString,
                builder => builder
                    .EnableRetryOnFailure()
                    .MigrationsAssembly("UniversityStructureMicroservice.Domain.Persistence.Ef")));

        return @this;
    }

    public static IServiceCollection RegisterRepositories(this IServiceCollection @this)
    {
        @this.AddScoped<IUniversityStructureContext, UniversityStructureContext>();

        @this.AddScoped<IFacultyRepository, FacultyRepository>();
        @this.AddScoped<IDepartmentRepository, DepartmentRepository>();
        @this.AddScoped<IDirectionRepository, DirectionRepository>();
        @this.AddScoped<IGroupRepository, GroupRepository>();
        @this.AddScoped<ISubjectRepository, SubjectRepository>();
        return @this;
    }

    public static IServiceCollection RegisterDomainEvents(this IServiceCollection @this)
    {
        return @this;
    }
}