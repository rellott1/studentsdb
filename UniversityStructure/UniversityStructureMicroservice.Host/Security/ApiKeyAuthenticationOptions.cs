﻿using Microsoft.AspNetCore.Authentication;

namespace UniversityStructureMicroservice.Host.Security;

public class ApiKeyAuthenticationOptions : AuthenticationSchemeOptions
{
    /// <summary>
    /// Allowed api keys and mapped user GUIDs
    /// </summary>
    public Dictionary<string, Guid> ApiKeys { get; set; }
    public string LesSecretKey { get; set; }
    public string LesAuthorizationHeader { get; set; }
}