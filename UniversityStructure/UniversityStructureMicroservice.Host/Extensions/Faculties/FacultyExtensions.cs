﻿using UniversityStructureMicroservice.Domain.Faculties;
using UniversityStructureMicroservice.Dto.Json.Faculties;

namespace UniversityStructureMicroservice.Host.Extensions.Faculties;

public static class FacultyExtensions
{
    public static FacultyDto ToJsonDto(this Faculty facultyEntity)
    {
        return new FacultyDto
        {
            Id = facultyEntity.FacultyId,
            Name = facultyEntity.Name
        };
    }

    public static FacultyDto[] ToJsonDto(this IEnumerable<Faculty> faculties)
    {
        return faculties.Select(at => at.ToJsonDto()).ToArray();
    }
}