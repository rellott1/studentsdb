﻿using UniversityStructureMicroservice.Domain.Departments;
using UniversityStructureMicroservice.Dto.Json.Departments;

namespace UniversityStructureMicroservice.Host.Extensions.Departments;

public static class DepartmentExtensions
{
    public static DepartmentDto ToJsonDto(this Department departmentEntity)
    {
        return new DepartmentDto
        {
            Id = departmentEntity.DepartmentId,
            Name = departmentEntity.Name,
            FacultyId = departmentEntity.FacultyId
        };
    }

    public static DepartmentDto[] ToJsonDto(this IEnumerable<Department> departments)
    {
        return departments.Select(at => at.ToJsonDto()).ToArray();
    }
}