﻿using UniversityStructureMicroservice.Domain.Directions;
using UniversityStructureMicroservice.Dto.Json.Directions;

namespace UniversityStructureMicroservice.Host.Extensions.Directions;

public static class DirectionExtensions
{
    public static DirectionDto ToJsonDto(this Direction directionEntity)
    {
        return new DirectionDto
        {
            Id = directionEntity.DirectionId,
            Name = directionEntity.Name,
			FacultyId = directionEntity.FacultyId
        };
    }

    public static DirectionDto[] ToJsonDto(this IEnumerable<Direction> directions)
    {
        return directions.Select(at => at.ToJsonDto()).ToArray();
    }
}