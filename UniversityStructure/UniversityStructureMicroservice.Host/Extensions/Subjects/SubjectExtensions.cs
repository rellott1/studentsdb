﻿using UniversityStructureMicroservice.Domain.Subjects;
using UniversityStructureMicroservice.Dto.Json.Subjects;

namespace UniversityStructureMicroservice.Host.Extensions.Subjects;

public static class SubjectExtensions
{
    public static SubjectDto ToJsonDto(this Subject subjectEntity)
    {
        return new SubjectDto
        {
            Id = subjectEntity.SubjectId,
            Name = subjectEntity.Name,
        };
    }

    public static SubjectDto[] ToJsonDto(this IEnumerable<Subject> subjects)
    {
        return subjects.Select(at => at.ToJsonDto()).ToArray();
    }
}