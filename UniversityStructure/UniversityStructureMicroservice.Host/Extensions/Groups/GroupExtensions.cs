﻿using UniversityStructureMicroservice.Domain.Groups;
using UniversityStructureMicroservice.Dto.Json.Groups;

namespace UniversityStructureMicroservice.Host.Extensions.Groups;

public static class GroupExtensions
{
    public static GroupDto ToJsonDto(this Group groupEntity)
    {
        return new GroupDto
        {
            Id = groupEntity.GroupId,
            Name = groupEntity.Name,
            Course = (int) groupEntity.Course,
            EnrollmentYear = groupEntity.EnrollmentYear,
            DirectionId = groupEntity.DirectionId
        };
    }

    public static GroupDto[] ToJsonDto(this IEnumerable<Group> groups)
    {
        return groups.Select(at => at.ToJsonDto()).ToArray();
    }
}