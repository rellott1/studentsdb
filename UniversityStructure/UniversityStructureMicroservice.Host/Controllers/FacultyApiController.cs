﻿using UniversityStructureMicroservice.Controllers;
using UniversityStructureMicroservice.Dto.Json.Faculties;
using UniversityStructureMicroservice.Host.Extensions.Faculties;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UniversityStructureMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/faculties")]
[Produces("application/json")]
public class FacultyApiController : ControllerBase
{
    private readonly FacultyController _facultyController;

    public FacultyApiController(FacultyController facultyController)
    {
        _facultyController = facultyController;
    }

    [ProducesResponseType(typeof(FacultyDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<FacultyDto[]>> GetFaculties()
    {
        var result = await _facultyController.GetFacultiesAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(FacultyDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<FacultyDto>> GetFaculty(int? facultyId)
    {
        var result = facultyId.HasValue
            ? await _facultyController.GetFacultyAsync(facultyId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(FacultyDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<FacultyDto>> CreateFaculty([FromBody] CreateFacultyDto createFacultyDto)
    {
        var result = await _facultyController.CreateFacultyAsync(createFacultyDto);
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{facultyId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<FacultyDto>> UpdateFacultyAsync(int facultyId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _facultyController.UpdateFacultyAsync(facultyId, patch);
        return request.ToJsonDto();
    }
    
    [HttpDelete("{facultyId}")]
    public async Task<ActionResult<bool>> DeleteFaculty(int? facultyId)
    {
        var result = facultyId.HasValue
            ? await _facultyController.RemoveFacultyAsync(facultyId.Value)
            : false;

        return result;
    }
}