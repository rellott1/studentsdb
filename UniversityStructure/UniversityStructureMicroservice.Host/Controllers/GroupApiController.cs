﻿using UniversityStructureMicroservice.Controllers;
using UniversityStructureMicroservice.Dto.Json.Groups;
using UniversityStructureMicroservice.Host.Extensions.Groups;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UniversityStructureMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/groups")]
[Produces("application/json")]
public class GroupApiController : ControllerBase
{
    private readonly GroupController _groupController;

    public GroupApiController(GroupController groupController)
    {
        _groupController = groupController;
    }

    [ProducesResponseType(typeof(GroupDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<GroupDto[]>> GetGroups()
    {
        var result = await _groupController.GetGroupsAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(GroupDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<GroupDto>> GetGroup(int? groupId)
    {
        var result = groupId.HasValue
            ? await _groupController.GetGroupAsync(groupId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(typeof(GroupDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("allGroupsOfDirection")]
    public async Task<ActionResult<GroupDto[]>> GetAllGroupsOfDirection(int? directionId)
    {
        var result = directionId.HasValue
            ? await _groupController.GetAllGroupsOfDirection(directionId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(GroupDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<GroupDto>> CreateGroup([FromBody] CreateGroupDto createGroupDto)
    {
        var result = await _groupController.CreateGroupAsync(createGroupDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{groupId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<GroupDto>> UpdateGroupAsync(int groupId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _groupController.UpdateGroupAsync(groupId, patch);
        if (request != null)
            return request.ToJsonDto();

        return BadRequest();
    }
    
    [HttpDelete("{groupId}")]
    public async Task<ActionResult<bool>> DeleteGroup(int? groupId)
    {
        var result = groupId.HasValue
            ? await _groupController.RemoveGroupAsync(groupId.Value)
            : false;

        return result;
    }
}