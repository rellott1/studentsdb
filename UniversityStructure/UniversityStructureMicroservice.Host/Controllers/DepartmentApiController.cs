﻿using UniversityStructureMicroservice.Controllers;
using UniversityStructureMicroservice.Dto.Json.Departments;
using UniversityStructureMicroservice.Host.Extensions.Departments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UniversityStructureMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/departments")]
[Produces("application/json")]
public class DepartmentApiController : ControllerBase
{
    private readonly DepartmentController _departmentController;

    public DepartmentApiController(DepartmentController departmentController)
    {
        _departmentController = departmentController;
    }

    [ProducesResponseType(typeof(DepartmentDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<DepartmentDto[]>> GetDepartments()
    {
        var result = await _departmentController.GetDepartmentsAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(DepartmentDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<DepartmentDto>> GetDepartment(int? departmentId)
    {
        var result = departmentId.HasValue
            ? await _departmentController.GetDepartmentAsync(departmentId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(DepartmentDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<DepartmentDto>> CreateDepartment([FromBody] CreateDepartmentDto createDepartmentDto)
    {
        var result = await _departmentController.CreateDepartmentAsync(createDepartmentDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{departmentId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<DepartmentDto>> UpdateDepartmentAsync(int departmentId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _departmentController.UpdateDepartmentAsync(departmentId, patch);
        if (request != null)
            return request.ToJsonDto();
        
        return BadRequest();
    }
    
    [HttpDelete("{departmentId}")]
    public async Task<ActionResult<bool>> DeleteDepartment(int? departmentId)
    {
        var result = departmentId.HasValue
            ? await _departmentController.RemoveDepartmentAsync(departmentId.Value)
            : false;

        return result;
    }
}