﻿using UniversityStructureMicroservice.Controllers;
using UniversityStructureMicroservice.Dto.Json.Subjects;
using UniversityStructureMicroservice.Host.Extensions.Subjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UniversityStructureMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/subjects")]
[Produces("application/json")]
public class SubjectApiController : ControllerBase
{
    private readonly SubjectController _subjectController;

    public SubjectApiController(SubjectController subjectController)
    {
        _subjectController = subjectController;
    }

    [ProducesResponseType(typeof(SubjectDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<SubjectDto[]>> GetSubjects()
    {
        var result = await _subjectController.GetSubjectsAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(SubjectDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<SubjectDto>> GetSubject(int? subjectId)
    {
        var result = subjectId.HasValue
            ? await _subjectController.GetSubjectAsync(subjectId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(SubjectDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<SubjectDto>> CreateSubject([FromBody] CreateSubjectDto createSubjectDto)
    {
        var result = await _subjectController.CreateSubjectAsync(createSubjectDto);
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{subjectId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<SubjectDto>> UpdateSubjectAsync(int subjectId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _subjectController.UpdateSubjectAsync(subjectId, patch);
        return request.ToJsonDto();
    }
    
    [HttpDelete("{subjectId}")]
    public async Task<ActionResult<bool>> DeleteSubject(int? subjectId)
    {
        var result = subjectId.HasValue
            ? await _subjectController.RemoveSubjectAsync(subjectId.Value)
            : false;

        return result;
    }
}