﻿using UniversityStructureMicroservice.Controllers;
using UniversityStructureMicroservice.Dto.Json.Directions;
using UniversityStructureMicroservice.Host.Extensions.Directions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UniversityStructureMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/directions")]
[Produces("application/json")]
public class DirectionApiController : ControllerBase
{
    private readonly DirectionController _directionController;

    public DirectionApiController(DirectionController directionController)
    {
        _directionController = directionController;
    }

    [ProducesResponseType(typeof(DirectionDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<DirectionDto[]>> GetDirections()
    {
        var result = await _directionController.GetDirectionsAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(DirectionDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<DirectionDto>> GetDirection(int? directionId)
    {
        var result = directionId.HasValue
            ? await _directionController.GetDirectionAsync(directionId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(typeof(DirectionDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("allDirectionsOfFaculty")]
    public async Task<ActionResult<DirectionDto[]>> GetAllDirectionsOfFaculty(int? facultyId)
    {
        var result = facultyId.HasValue
            ? await _directionController.GetAllDirectionsOfFaculty(facultyId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(DirectionDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<DirectionDto>> CreateDirection([FromBody] CreateDirectionDto createDirectionDto)
    {
        var result = await _directionController.CreateDirectionAsync(createDirectionDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{directionId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<DirectionDto>> UpdateDirectionAsync(int directionId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _directionController.UpdateDirectionAsync(directionId, patch);
        if (request != null)
            return request.ToJsonDto();

        return BadRequest();
    }
    
    [HttpDelete("{directionId}")]
    public async Task<ActionResult<bool>> DeleteDirection(int? directionId)
    {
        var result = directionId.HasValue
            ? await _directionController.RemoveDirectionAsync(directionId.Value)
            : false;

        return result;
    }
}