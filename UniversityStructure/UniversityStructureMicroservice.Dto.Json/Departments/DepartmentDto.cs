﻿namespace UniversityStructureMicroservice.Dto.Json.Departments;

public class DepartmentDto : CreateDepartmentDto
{
    public int Id { get; set; }
}