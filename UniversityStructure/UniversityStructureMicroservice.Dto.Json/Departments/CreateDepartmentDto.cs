﻿namespace UniversityStructureMicroservice.Dto.Json.Departments;

public class CreateDepartmentDto
{
    public string Name { get; set; }
    public int FacultyId { get; set; }
}