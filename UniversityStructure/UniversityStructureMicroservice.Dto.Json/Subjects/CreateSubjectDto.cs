﻿namespace UniversityStructureMicroservice.Dto.Json.Subjects;

public class CreateSubjectDto
{
    public string Name { get; set; }
}