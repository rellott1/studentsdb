﻿namespace UniversityStructureMicroservice.Dto.Json.Subjects;

public class SubjectDto : CreateSubjectDto
{
    public int Id { get; set; }
}