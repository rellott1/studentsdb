﻿namespace UniversityStructureMicroservice.Dto.Json.Faculties;

public class FacultyDto : CreateFacultyDto
{
    public int Id { get; set; }
}