﻿namespace UniversityStructureMicroservice.Dto.Json.Faculties;

public class CreateFacultyDto
{
    public string Name { get; set; }
}