﻿namespace UniversityStructureMicroservice.Dto.Json.Groups;

public class CreateGroupDto
{
    public string Name { get; set; }
    public int Course { get; set; }
    public DateTime EnrollmentYear { get; set; }
    public int DirectionId { get; set; }
}