﻿namespace UniversityStructureMicroservice.Dto.Json.Groups;

public class GroupDto : CreateGroupDto
{
    public int Id { get; set; }
}