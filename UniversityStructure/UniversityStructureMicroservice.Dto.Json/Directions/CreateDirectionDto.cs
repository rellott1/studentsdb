﻿namespace UniversityStructureMicroservice.Dto.Json.Directions;

public class CreateDirectionDto
{
    public string Name { get; set; }
    public int FacultyId { get; set; }
}