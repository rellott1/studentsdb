﻿namespace UniversityStructureMicroservice.Dto.Json.Directions;

public class DirectionDto : CreateDirectionDto
{
    public int Id { get; set; }
}