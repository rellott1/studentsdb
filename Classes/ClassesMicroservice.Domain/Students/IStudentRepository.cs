﻿using ClassesMicroservice.Dto.Json.Students;
using Microsoft.AspNetCore.JsonPatch;

namespace ClassesMicroservice.Domain.Students;

public interface IStudentRepository
{
    Task<List<Student>> GetStudentsAsync();
    Task<Student> GetStudentAsync(int studentId);
    Task<Student> CreateStudentAsync(CreateStudentDto createStudentDto);
    Task<Student> UpdateStudentAsync(int studentId, JsonPatchDocument patch);
    Task<bool> RemoveStudentAsync(int studentId);
    Task<List<Student>> GetAllStudentsFromGroupAsync(int groupId);
}