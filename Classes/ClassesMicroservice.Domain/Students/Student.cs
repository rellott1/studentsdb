﻿using ClassesMicroservice.Domain.Users;

namespace ClassesMicroservice.Domain.Students;

public class Student
{
    public int StudentId { get; set; }
    public string FullName { get; set; }
    public int GroupId { get; set; }
    public int? UserId { get; set; }
    public virtual User User { get; set; }
}