﻿using ClassesMicroservice.Domain.Teachers;

namespace ClassesMicroservice.Domain.Classes;

public class Class
{
    public int ClassId { get; set; }
    public bool IsRemote { get; set; }
    public string Classroom { get; set; }
    public DateTime Date { get; set; }
    public LessonType LessonType { get; set; }
    public int WeekNumber { get; set; }
    public int StudyHour { get; set; }
    public int SubjectId { get; set; }
    public int? TeacherId { get; set; }
    public virtual Teacher Teacher { get; set; }
    
    public ICollection<ClassGroup> ClassGroups { get; set; }
}