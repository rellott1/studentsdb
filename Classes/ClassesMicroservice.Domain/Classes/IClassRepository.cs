﻿using ClassesMicroservice.Dto.Json.Classes;
using Microsoft.AspNetCore.JsonPatch;

namespace ClassesMicroservice.Domain.Classes;

public interface IClassRepository
{
    Task<List<Class>> GetClassesAsync();
    Task<Class> GetClassAsync(int classId);
    Task<Class> CreateClassAsync(CreateClassDto createClassDto);
    Task<Class> UpdateClassAsync(int classId, JsonPatchDocument patch);
    Task<bool> RemoveClassAsync(int classId);
    Task<List<Class>> GetAllClassesInOddWeekAsync();
}