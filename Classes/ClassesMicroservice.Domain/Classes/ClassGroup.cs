﻿namespace ClassesMicroservice.Domain.Classes;

public class ClassGroup
{
    public int Id { get; set; }
    public int ClassId { get; set; }
    public int GroupId { get; set; }
    
    public virtual Class Class { get; set; }
}