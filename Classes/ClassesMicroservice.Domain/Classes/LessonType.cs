﻿namespace ClassesMicroservice.Domain.Classes;

public enum LessonType : byte
{
    Lecture = 1,
    Practice = 2,
    LaboratoryWork = 3
}