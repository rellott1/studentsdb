﻿using ClassesMicroservice.Dto.Json.Teachers;
using Microsoft.AspNetCore.JsonPatch;

namespace ClassesMicroservice.Domain.Teachers;

public interface ITeacherRepository
{
    Task<List<Teacher>> GetTeachersAsync();
    Task<Teacher> GetTeacherAsync(int teacherId);
    Task<Teacher> CreateTeacherAsync(CreateTeacherDto createTeacherDto);
    Task<Teacher> UpdateTeacherAsync(int teacherId, JsonPatchDocument patch);
    Task<bool> RemoveTeacherAsync(int teacherId);
}