﻿using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Domain.Classes;

namespace ClassesMicroservice.Domain.Teachers;

public class Teacher
{
    public int TeacherId { get; set; }
    public string FullName { get; set; }
    public string AcademicDegree { get; set; }
    public int DepartmentId { get; set; }
    public int? UserId { get; set; }
    public virtual User User { get; set; }
    public ICollection<Class> Classes { get; set; }
}