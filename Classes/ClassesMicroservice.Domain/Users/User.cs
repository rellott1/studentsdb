﻿using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Domain.Students;

namespace ClassesMicroservice.Domain.Users;

public class User
{
    public int UserId { get; set; } 
    public string Login { get; set; }
    public string Password { get; set; }
    public Role Role { get; set; }
    public ICollection<Teacher> Teachers { get; set; }
    public ICollection<Student> Students { get; set; }
}