﻿namespace ClassesMicroservice.Domain.Users;

public enum Role : byte
{
    Admin = 1,
    Teacher = 2,
    Student = 3
}