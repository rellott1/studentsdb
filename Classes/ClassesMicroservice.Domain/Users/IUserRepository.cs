﻿using ClassesMicroservice.Dto.Json.Users;
using Microsoft.AspNetCore.JsonPatch;

namespace ClassesMicroservice.Domain.Users;

public interface IUserRepository
{
    Task<List<User>> GetUsersAsync();
    Task<User> GetUserAsync(int userId);
    Task<User> CreateUserAsync(CreateUserDto createUserDto);
    Task<User> UpdateUserAsync(int userId, JsonPatchDocument patch);
    Task<bool> RemoveUserAsync(int userId);
}