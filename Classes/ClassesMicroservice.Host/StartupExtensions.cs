﻿using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using ClassesMicroservice.Controllers;
using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Domain.Students;
using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Domain.Classes;
using ClassesMicroservice.Domain.Persistence.Ef;
using ClassesMicroservice.Domain.Persistence.Ef.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: InternalsVisibleTo("ClassesMicroservice.Host")]
namespace ClassesMicroservice.Host;

public static class StartupExtensions
{
    public static IServiceCollection RegisterApplicationControllers(this IServiceCollection @this)
    {
        @this.AddScoped<UserController, UserController>();
        @this.AddScoped<StudentController, StudentController>();
        @this.AddScoped<TeacherController, TeacherController>();
        @this.AddScoped<ClassController, ClassController>();
        return @this;
    }
    
    public static IServiceCollection RegisterServices(this IServiceCollection @this)
    {
        return @this;
    }
    
    public static IServiceCollection RegisterDatabase(this IServiceCollection @this, string connectionString)
    {
        @this.AddDbContext<ClassesContext>(options =>
            options.UseNpgsql(connectionString, 
                builder => builder
                    .EnableRetryOnFailure()
                    .MigrationsAssembly("ClassesMicroservice.Domain.Persistence.Ef")));

        return @this;
    }

    public static IServiceCollection RegisterRepositories(this IServiceCollection @this)
    {
        @this.AddScoped<IClassesContext, ClassesContext>();

        @this.AddScoped<IUserRepository, UserRepository>();
        @this.AddScoped<IStudentRepository, StudentRepository>();
        @this.AddScoped<ITeacherRepository, TeacherRepository>();
        @this.AddScoped<IClassRepository, ClassRepository>();
        return @this;
    }

    public static IServiceCollection RegisterDomainEvents(this IServiceCollection @this)
    {
        return @this;
    }
}