﻿using Microsoft.AspNetCore.Authentication;

namespace ClassesMicroservice.Host.Security;

public static class ApiKeyAuthenticationExtensions
{
    public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder)
        => builder.AddApiKey(ApiKeyAuthenticationDefaults.AuthenticationScheme, _ => { });

    public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, Action<ApiKeyAuthenticationOptions> configureOptions)
        => builder.AddApiKey(ApiKeyAuthenticationDefaults.AuthenticationScheme, configureOptions);

    public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, string authenticationScheme, Action<ApiKeyAuthenticationOptions> configureOptions)
        => builder.AddApiKey(authenticationScheme, displayName: null, configureOptions: configureOptions);

    public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, string authenticationScheme,
        string displayName, Action<ApiKeyAuthenticationOptions> configureOptions)
        => builder.AddScheme<ApiKeyAuthenticationOptions, ApiKeyAuthenticationHandler>(authenticationScheme, displayName, configureOptions);
}