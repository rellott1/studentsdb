﻿using System;
using System.Security.Claims;
using System.Text.Encodings.Web;
using ClassesMicroservice.Host.Clients;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ClassesMicroservice.Host.Security;

public class ApiKeyAuthenticationHandler : AuthenticationHandler<ApiKeyAuthenticationOptions>
{
    private readonly IMemoryCache _memoryCache;
    private readonly ClassesClient _client;
    
    public IConfiguration Configuration { get; }
    
    public ApiKeyAuthenticationHandler(IOptionsMonitor<ApiKeyAuthenticationOptions> options, ILoggerFactory logger,
        UrlEncoder encoder, ISystemClock clock, IMemoryCache memoryCache, ClassesClient client,
        IConfiguration configuration) : base(options,
        logger, encoder, clock)
    {
        _memoryCache = memoryCache;
        _client = client;
        Configuration = configuration;
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        var authorization = (string) Request.Headers[Configuration["SecurityOptions:AuthorizationHeader"]];
        var apiKey = authorization;

        if (string.IsNullOrEmpty(apiKey))
        {
            // No api key
            return await Task.FromResult(AuthenticateResult.NoResult());
        }

        var apiKeyVerification = await _memoryCache.GetOrCreateAsync(apiKey, async entry =>
        {
            // Cache miss - check in database and cache the result
            entry.SetAbsoluteExpiration(TimeSpan.FromMinutes(1));
            return await _client.VerifyApiKey(apiKey);
        });
        
        if (!apiKeyVerification.IsValid)
        {
            // Provided api key does not match any key in options
            return await Task.FromResult(AuthenticateResult.Fail("Invalid API Key"));
        }
        if (!apiKeyVerification.UserGuid.HasValue)
        {
            // Provided api key does not match any key in options
            return await Task.FromResult(AuthenticateResult.Fail("Missing UserGuid in auth client response"));
        }
        
        // Api key ok, authenticate request using a dummy principal
        var claims = new []
        {
            new Claim(ClaimTypes.NameIdentifier, apiKeyVerification.UserGuid.ToString())
        };
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, Scheme.Name));
        var ticket = new AuthenticationTicket(principal, null, Scheme.Name);
        return await Task.FromResult(AuthenticateResult.Success(ticket));
    }
}