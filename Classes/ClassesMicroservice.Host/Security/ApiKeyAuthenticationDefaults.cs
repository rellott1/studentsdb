﻿namespace ClassesMicroservice.Host.Security;

public static class ApiKeyAuthenticationDefaults
{
    /// <summary>
    /// The default value used for AuthenticationScheme
    /// </summary>
    public const string AuthenticationScheme = "ApiKey";
}