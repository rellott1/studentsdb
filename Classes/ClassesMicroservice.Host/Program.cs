using ClassesMicroservice.Host;
using ClassesMicroservice.Host.Clients;
using ClassesMicroservice.Host.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
builder.Services
    .AddAuthentication(ApiKeyAuthenticationDefaults.AuthenticationScheme)
    .AddApiKey();

builder.Services
    .AddAuthorization()
    .AddMemoryCache()
    .AddControllers()
    .AddNewtonsoftJson();

builder.Services
    .AddHttpClient<ClassesClient>()
    .ConfigureHttpClient((provider, httpClient) =>
    {
        httpClient.BaseAddress = new Uri("http://192.168.250.65");
    });

builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Version = "v1",
            Title = "Classes API",
            Description = "API for working with the university classes",
            Contact = new OpenApiContact
            {
                Name = "Alexander Rublev",
                Email = "alexander.rublev@aug-e.io"
            },
        });
        c.AddSecurityDefinition("ApiKey", new OpenApiSecurityScheme()
        {
            Type = SecuritySchemeType.ApiKey,
            In = ParameterLocation.Header,
            Name = builder.Configuration["SecurityOptions:AuthorizationHeader"],
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "ApiKey" }
                },
                new string[] { }
            }
        });
    }
);

var databaseConnectionString = builder.Configuration["ConnectionStrings:ClassesDb"];

builder.Services
    .AddHttpContextAccessor()
    .RegisterDatabase(databaseConnectionString)
    .RegisterRepositories()
    .RegisterApplicationControllers();

var app = builder.Build();

app
    .UseHttpsRedirection()
    .UseRouting()
    .UseAuthentication()
    .UseAuthorization()
    .UseEndpoints(endpoints => { endpoints.MapControllers(); })
    .UseSwagger()
    .UseSwaggerUI();

app.Run();