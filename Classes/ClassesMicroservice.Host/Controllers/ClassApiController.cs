﻿using ClassesMicroservice.Controllers;
using ClassesMicroservice.Dto.Json.Classes;
using ClassesMicroservice.Host.Extensions.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClassesMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/classes")]
[Produces("application/json")]
public class ClassApiController : ControllerBase
{
    private readonly ClassController _classController;

    public ClassApiController(ClassController classController)
    {
        _classController = classController;
    }
    
    [ProducesResponseType(typeof(ClassDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<ClassDto[]>> GetClasses()
    {
        var result = await _classController.GetClassesAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(ClassDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<ClassDto>> GetClass(int? classId)
    {
        var result = classId.HasValue
            ? await _classController.GetClassAsync(classId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(typeof(ClassDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("allClassesInOddWeek")]
    public async Task<ActionResult<ClassDto[]>> GetAllClassesInOddWeek()
    {
        var result = await _classController.GetAllClassesInOddWeekAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(ClassDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<ClassDto>> CreateClass([FromBody] CreateClassDto createClassDto)
    {
        var result = await _classController.CreateClassAsync(createClassDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{classId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<ClassDto>> UpdateClassAsync(int classId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _classController.UpdateClassAsync(classId, patch);
        if (request != null)
            return request.ToJsonDto();

        return BadRequest();
    }
    
    [HttpDelete("{classId}")]
    public async Task<ActionResult<bool>> DeleteClass(int? classId)
    {
        var result = classId.HasValue
            ? await _classController.RemoveClassAsync(classId.Value)
            : false;

        return result;
    }
}