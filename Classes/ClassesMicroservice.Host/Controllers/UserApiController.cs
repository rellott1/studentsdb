﻿using ClassesMicroservice.Controllers;
using ClassesMicroservice.Dto.Json.Users;
using ClassesMicroservice.Host.Extensions.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClassesMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/users")]
[Produces("application/json")]
public class UserApiController : ControllerBase
{
    private readonly UserController _userController;

    public UserApiController(UserController userController)
    {
        _userController = userController;
    }
    
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<UserDto[]>> GetUsers()
    {
        var result = await _userController.GetUsersAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<UserDto>> GetUser(int? userId)
    {
        var result = userId.HasValue
            ? await _userController.GetUserAsync(userId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<UserDto>> CreateUser([FromBody] CreateUserDto createUserDto)
    {
        var result = await _userController.CreateUserAsync(createUserDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{userId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<UserDto>> UpdateUserAsync(int userId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _userController.UpdateUserAsync(userId, patch);
        if (request != null)
            return request.ToJsonDto();
        
        return BadRequest();
    }
    
    [HttpDelete("{userId}")]
    public async Task<ActionResult<bool>> DeleteUser(int? userId)
    {
        var result = userId.HasValue
            ? await _userController.RemoveUserAsync(userId.Value)
            : false;

        return result;
    }
}