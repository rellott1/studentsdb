﻿using System.Net;
using ClassesMicroservice.Controllers;
using ClassesMicroservice.Dto.Json.Students;
using ClassesMicroservice.Host.Extensions.Students;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClassesMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/students")]
[Produces("application/json")]
public class StudentApiController : ControllerBase
{
    private readonly StudentController _studentController;

    public StudentApiController(StudentController studentController)
    {
        _studentController = studentController;
    }
    
    private async Task<bool> GetGroupPrecense(string groupId)
    {
        var url = $"https://localhost:7080/v1/groups?groupId={groupId}";
        var httpRequest = (HttpWebRequest) WebRequest.Create(url);
        httpRequest.Headers["X-SM-Api-Key"] = "777";
        var httpResponse = (HttpWebResponse) await httpRequest.GetResponseAsync();

        return httpResponse.StatusCode != HttpStatusCode.NoContent;
    }
    
    [ProducesResponseType(typeof(StudentDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<StudentDto[]>> GetStudents()
    {
        var result = await _studentController.GetStudentsAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(StudentDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<StudentDto>> GetStudent(int? studentId)
    {
        var result = studentId.HasValue
            ? await _studentController.GetStudentAsync(studentId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(typeof(StudentDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("allStudentsFromGroup")]
    public async Task<ActionResult<StudentDto[]>> GetAllStudentsFromGroup(int groupId)
    {
        var result = await _studentController.GetAllStudentsFromGroupAsync(groupId);
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(StudentDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<StudentDto>> CreateStudent([FromBody] CreateStudentDto createStudentDto)
    {
        bool exists = await GetGroupPrecense(createStudentDto.GroupId.ToString());
        
        if (!exists)
            return BadRequest();

        var result = await _studentController.CreateStudentAsync(createStudentDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{studentId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<StudentDto>> UpdateStudentAsync(int studentId, [FromBody]JsonPatchDocument patch)
    {
        if (patch.Operations[0].op == "replace" && patch.Operations[0].path == "/groupId")
        {
            bool exists = await GetGroupPrecense(patch.Operations[0].value.ToString());

            if (!exists)
                return BadRequest();
        }
        
        var request = await _studentController.UpdateStudentAsync(studentId, patch);
        if (request != null) 
            return request.ToJsonDto();

        return BadRequest();
    }
    
    [HttpDelete("{studentId}")]
    public async Task<ActionResult<bool>> DeleteStudent(int? studentId)
    {
        var result = studentId.HasValue
            ? await _studentController.RemoveStudentAsync(studentId.Value)
            : false;

        return result;
    }
}