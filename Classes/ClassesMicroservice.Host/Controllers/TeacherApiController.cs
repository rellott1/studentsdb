﻿using System.Net;
using ClassesMicroservice.Controllers;
using ClassesMicroservice.Dto.Json.Teachers;
using ClassesMicroservice.Host.Extensions.Teachers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClassesMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/teachers")]
[Produces("application/json")]
public class TeacherApiController : ControllerBase
{
    private readonly TeacherController _teacherController;

    public TeacherApiController(TeacherController teacherController)
    {
        _teacherController = teacherController;
    }
    
    private async Task<bool> GetDepartmentPrecense(string departmentId)
    {
        var url = $"https://localhost:7080/v1/departments?departmentId={departmentId}";
        var httpRequest = (HttpWebRequest) WebRequest.Create(url);
        httpRequest.Headers["X-SM-Api-Key"] = "777";
        var httpResponse = (HttpWebResponse) await httpRequest.GetResponseAsync();

        return httpResponse.StatusCode != HttpStatusCode.NoContent;
    }
    
    [ProducesResponseType(typeof(TeacherDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<TeacherDto[]>> GetTeachers()
    {
        var result = await _teacherController.GetTeachersAsync();
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(TeacherDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<TeacherDto>> GetTeacher(int? teacherId)
    {
        var result = teacherId.HasValue
            ? await _teacherController.GetTeacherAsync(teacherId.Value)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(TeacherDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<TeacherDto>> CreateTeacher([FromBody] CreateTeacherDto createTeacherDto)
    {
        bool exists = await GetDepartmentPrecense(createTeacherDto.DepartmentId.ToString());
        
        if (!exists)
            return BadRequest();
        
        var result = await _teacherController.CreateTeacherAsync(createTeacherDto);
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{teacherId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<TeacherDto>> UpdateTeacherAsync(int teacherId, [FromBody]JsonPatchDocument patch)
    {
        if (patch.Operations[0].op == "replace" && patch.Operations[0].path == "/departmentId")
        {
            bool exists = await GetDepartmentPrecense(patch.Operations[0].value.ToString());

            if (!exists)
                return BadRequest();
        }
        
        var request = await _teacherController.UpdateTeacherAsync(teacherId, patch);
        if (request != null)
            return request.ToJsonDto();

        return BadRequest();
    }
    
    [HttpDelete("{teacherId}")]
    public async Task<ActionResult<bool>> DeleteTeacher(int? teacherId)
    {
        var result = teacherId.HasValue
            ? await _teacherController.RemoveTeacherAsync(teacherId.Value)
            : false;

        return result;
    }
}