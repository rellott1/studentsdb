﻿using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Dto.Json.Teachers;

namespace ClassesMicroservice.Host.Extensions.Teachers;

public static class TeacherExtensions
{
    public static TeacherDto ToJsonDto(this Teacher teacherEntity)
    {
        return new TeacherDto
        {
            Id = teacherEntity.TeacherId,
            FullName = teacherEntity.FullName,
            AcademicDegree = teacherEntity.AcademicDegree,
            DepartmentId = teacherEntity.DepartmentId,
            UserId = teacherEntity.UserId.Value
        };
    }

    public static TeacherDto[] ToJsonDto(this IEnumerable<Teacher> teachers)
    {
        return teachers.Select(at => at.ToJsonDto()).ToArray();
    }
}