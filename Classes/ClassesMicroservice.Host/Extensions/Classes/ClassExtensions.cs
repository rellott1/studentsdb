﻿using ClassesMicroservice.Domain.Classes;
using ClassesMicroservice.Dto.Json.Classes;

namespace ClassesMicroservice.Host.Extensions.Classes;

public static class ClassExtensions
{
    public static ClassDto ToJsonDto(this Class classEntity)
    {
        return new ClassDto
        {
            Id = classEntity.ClassId,
            IsRemote = classEntity.IsRemote,
            Classroom = classEntity.Classroom,
            Date = classEntity.Date,
            LessonType = (int) classEntity.LessonType,
            WeekNumber = classEntity.WeekNumber,
            StudyHour = classEntity.StudyHour,
            SubjectId = classEntity.SubjectId,
            TeacherId = classEntity.TeacherId
        };
    }

    public static ClassDto[] ToJsonDto(this IEnumerable<Class> classes)
    {
        return classes.Select(at => at.ToJsonDto()).ToArray();
    }
}