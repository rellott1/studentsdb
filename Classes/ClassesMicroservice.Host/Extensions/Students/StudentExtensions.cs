﻿using ClassesMicroservice.Domain.Students;
using ClassesMicroservice.Dto.Json.Students;

namespace ClassesMicroservice.Host.Extensions.Students;

public static class StudentExtensions
{
    public static StudentDto ToJsonDto(this Student studentEntity)
    {
        return new StudentDto
        {
            Id = studentEntity.StudentId,
            FullName = studentEntity.FullName,
            GroupId = studentEntity.GroupId,
            UserId = studentEntity.UserId.Value
        };
    }

    public static StudentDto[] ToJsonDto(this IEnumerable<Student> students)
    {
        return students.Select(at => at.ToJsonDto()).ToArray();
    }
}