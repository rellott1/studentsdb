﻿using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Dto.Json.Users;

namespace ClassesMicroservice.Host.Extensions.Users;

public static class UserExtensions
{
    public static UserDto ToJsonDto(this User userEntity)
    {
        return new UserDto
        {
            Id = userEntity.UserId,
            Login = userEntity.Login,
            Password = userEntity.Password,
            Role = (int) userEntity.Role
        };
    }

    public static UserDto[] ToJsonDto(this IEnumerable<User> users)
    {
        return users.Select(at => at.ToJsonDto()).ToArray();
    }
}