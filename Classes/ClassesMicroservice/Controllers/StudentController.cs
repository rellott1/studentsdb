﻿using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Students;
using ClassesMicroservice.Dto.Json.Students;

namespace ClassesMicroservice.Controllers;

public class StudentController
{
    private readonly IStudentRepository _studentRepository;

    public StudentController(IStudentRepository studentRepository)
    {
        _studentRepository = studentRepository;
    }

    public async Task<List<Student>> GetStudentsAsync()
    {
        return await _studentRepository.GetStudentsAsync();
    }

    public async Task<Student> GetStudentAsync(int studentId)
    {
        return await _studentRepository.GetStudentAsync(studentId);
    }
    
    public async Task<Student> CreateStudentAsync(CreateStudentDto createStudentDto)
    {
        return await _studentRepository.CreateStudentAsync(createStudentDto);
    }

    public async Task<Student> UpdateStudentAsync(int studentId, JsonPatchDocument patch)
    {
        return await _studentRepository.UpdateStudentAsync(studentId, patch);
    }

    public async Task<bool> RemoveStudentAsync(int studentId)
    {
        return await _studentRepository.RemoveStudentAsync(studentId);
    }

    public async Task<List<Student>> GetAllStudentsFromGroupAsync(int groupId)
    {
        return await _studentRepository.GetAllStudentsFromGroupAsync(groupId);
    }
}
