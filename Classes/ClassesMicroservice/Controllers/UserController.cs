﻿using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Dto.Json.Users;

namespace ClassesMicroservice.Controllers;

public class UserController
{
    private readonly IUserRepository _userRepository;

    public UserController(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<List<User>> GetUsersAsync()
    {
        return await _userRepository.GetUsersAsync();
    }

    public async Task<User> GetUserAsync(int userId)
    {
        return await _userRepository.GetUserAsync(userId);
    }

    public async Task<User> CreateUserAsync(CreateUserDto createUserDto)
    {
        return await _userRepository.CreateUserAsync(createUserDto);
    }

    public async Task<User> UpdateUserAsync(int userId, JsonPatchDocument patch)
    {
        return await _userRepository.UpdateUserAsync(userId, patch);
    }

    public async Task<bool> RemoveUserAsync(int userId)
    {
        return await _userRepository.RemoveUserAsync(userId);
    }
}