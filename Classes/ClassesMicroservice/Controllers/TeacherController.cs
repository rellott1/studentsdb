﻿using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Dto.Json.Teachers;

namespace ClassesMicroservice.Controllers;

public class TeacherController
{
    private readonly ITeacherRepository _teacherRepository;

    public TeacherController(ITeacherRepository teacherRepository)
    {
        _teacherRepository = teacherRepository;
    }

    public async Task<List<Teacher>> GetTeachersAsync()
    {
        return await _teacherRepository.GetTeachersAsync();
    }

    public async Task<Teacher> GetTeacherAsync(int teacherId)
    {
        return await _teacherRepository.GetTeacherAsync(teacherId);
    }
    
    public async Task<Teacher> CreateTeacherAsync(CreateTeacherDto createTeacherDto)
    {
        return await _teacherRepository.CreateTeacherAsync(createTeacherDto);
    }

    public async Task<Teacher> UpdateTeacherAsync(int teacherId, JsonPatchDocument patch)
    {
        return await _teacherRepository.UpdateTeacherAsync(teacherId, patch);
    }

    public async Task<bool> RemoveTeacherAsync(int teacherId)
    {
        return await _teacherRepository.RemoveTeacherAsync(teacherId);
    }
}