﻿using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Classes;
using ClassesMicroservice.Dto.Json.Classes;

namespace ClassesMicroservice.Controllers;

public class ClassController
{
    private readonly IClassRepository _classRepository;

    public ClassController(IClassRepository classRepository)
    {
        _classRepository = classRepository;
    }

    public async Task<List<Class>> GetClassesAsync()
    {
        return await _classRepository.GetClassesAsync();
    }

    public async Task<Class> GetClassAsync(int classId)
    {
        return await _classRepository.GetClassAsync(classId);
    }
    
    public async Task<Class> CreateClassAsync(CreateClassDto createClassDto)
    {
        return await _classRepository.CreateClassAsync(createClassDto);
    }

    public async Task<Class> UpdateClassAsync(int classId, JsonPatchDocument patch)
    {
        return await _classRepository.UpdateClassAsync(classId, patch);
    }

    public async Task<bool> RemoveClassAsync(int classId)
    {
        return await _classRepository.RemoveClassAsync(classId);
    }

    public async Task<List<Class>> GetAllClassesInOddWeekAsync()
    {
        return await _classRepository.GetAllClassesInOddWeekAsync();
    }
}