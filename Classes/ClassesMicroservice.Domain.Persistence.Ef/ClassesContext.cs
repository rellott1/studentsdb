﻿using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Domain.Students;
using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Domain.Classes;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using ClassesMicroservice.Domain.Persistence.Ef.Extensions;

namespace ClassesMicroservice.Domain.Persistence.Ef;

public class ClassesContext : DbContext, IClassesContext
{
    public ClassesContext(DbContextOptions<ClassesContext> options)
        : base(options)
    {
    }
    
    public DbSet<User> Users { get; set; }
    public DbSet<Student> Students { get; set; }
    public DbSet<Teacher> Teachers { get; set; }
    public DbSet<Class> Classes { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyEfConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}