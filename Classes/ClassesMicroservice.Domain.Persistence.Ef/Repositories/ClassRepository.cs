﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Classes;
using ClassesMicroservice.Dto.Json.Classes;
using Microsoft.EntityFrameworkCore;

namespace ClassesMicroservice.Domain.Persistence.Ef.Repositories;

public class ClassRepository : EfRepository, IClassRepository
{
    public ClassRepository(IClassesContext context) : base(context)
    {
    }

    public async Task<List<Class>> GetClassesAsync()
    {
        return await Context.Classes.ToListAsync();
    }

    public async Task<Class> GetClassAsync(int classId)
    {
        return await Context.Classes.FirstOrDefaultAsync(q => q.ClassId == classId);
    }

    public async Task<Class> CreateClassAsync(CreateClassDto createClassDto)
    {
        if (!Enum.IsDefined(typeof(LessonType), (byte) createClassDto.LessonType))
            return null;
        
        if (createClassDto.StudyHour < 1 && createClassDto.StudyHour > 6)
            return null;
        
        if (createClassDto.WeekNumber < 1 && createClassDto.WeekNumber > 2)
            return null;

        var classEntity = new Class
        {
            IsRemote = createClassDto.IsRemote,
			Classroom = createClassDto.Classroom,
            Date = createClassDto.Date,
            LessonType = (LessonType) createClassDto.LessonType,
            WeekNumber = createClassDto.WeekNumber,
            StudyHour = createClassDto.StudyHour,
            SubjectId = createClassDto.SubjectId,
            TeacherId = createClassDto.TeacherId
        };
        await Context.Classes.AddAsync(classEntity);
        await Context.SaveChangesAsync();
        return classEntity;
    }

    public async Task<Class> UpdateClassAsync(int classId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/lessonType" && patch.Operations[0].op == "replace")
        {
            if (!Enum.IsDefined(typeof(LessonType), Convert.ToByte(patch.Operations[0].value)))
                return null;
        }
        
        if (patch.Operations[0].path == "/studyHour" && patch.Operations[0].op == "replace")
        {
            var studyHour = (int)patch.Operations[0].value;
            if (studyHour < 1 && studyHour > 6)
                return null;
        }
        
        if (patch.Operations[0].path == "/weekNumber" && patch.Operations[0].op == "replace")
        {
            var weekNumber = (int)patch.Operations[0].value;
            if (weekNumber < 1 && weekNumber > 2)
                return null;
        }
        
        var classEntity = await Context.Classes
            .SingleOrDefaultAsync(q => q.ClassId == classId);

        if (classEntity == null)
            throw new Exception("Class does not exist");
        
        patch.ApplyTo(classEntity);
        await Context.SaveChangesAsync();
        return classEntity;
    }

    public async Task<bool> RemoveClassAsync(int classId)
    {
        var classEntity = await Context.Classes
            .SingleOrDefaultAsync(q => q.ClassId == classId);

        if (classEntity == null)
        {
            // throw new Exception("Class does not exist");
            return false;
        }

        Context.Classes.Remove(classEntity);
        await Context.SaveChangesAsync();
        return true;
    }
    
    public async Task<List<Class>> GetAllClassesInOddWeekAsync()
    {
        return await Context.Classes.Where(q => q.WeekNumber == 1).ToListAsync();
    }
}