﻿namespace ClassesMicroservice.Domain.Persistence.Ef.Repositories;

public class EfRepository
{
    protected readonly IClassesContext Context;

    protected EfRepository(IClassesContext context)
    {
        Context = context;
    }
}