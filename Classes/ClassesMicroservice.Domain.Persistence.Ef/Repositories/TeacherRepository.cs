﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Dto.Json.Teachers;
using Microsoft.EntityFrameworkCore;

namespace ClassesMicroservice.Domain.Persistence.Ef.Repositories;

public class TeacherRepository : EfRepository, ITeacherRepository
{
    public TeacherRepository(IClassesContext context) : base(context)
    {
    }

    public async Task<List<Teacher>> GetTeachersAsync()
    {
        return await Context.Teachers.ToListAsync();
    }

    public async Task<Teacher> GetTeacherAsync(int teacherId)
    {
        return await Context.Teachers.FirstOrDefaultAsync(q => q.TeacherId == teacherId);
    }

    public async Task<Teacher> CreateTeacherAsync(CreateTeacherDto createTeacherDto)
    {
        if (createTeacherDto.UserId.HasValue)
        {
            var user = await Context.Users
                .FirstOrDefaultAsync(q => q.UserId == createTeacherDto.UserId.Value);

            if (user == null)
                return null;
        }
        
        var teacherEntity = new Teacher
        {
            FullName = createTeacherDto.FullName,
			AcademicDegree = createTeacherDto.AcademicDegree,
            DepartmentId = createTeacherDto.DepartmentId,
            UserId = createTeacherDto.UserId
        };
        await Context.Teachers.AddAsync(teacherEntity);
        await Context.SaveChangesAsync();
        return teacherEntity;
    }

    public async Task<Teacher> UpdateTeacherAsync(int teacherId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/userId" && patch.Operations[0].op == "replace")
        {
            var user = await Context.Users
                .FirstOrDefaultAsync(q => q.UserId == (int) patch.Operations[0].value);

            if (user == null)
                return null;
        }
        
        var teacher = await Context.Teachers
            .SingleOrDefaultAsync(q => q.TeacherId == teacherId);

        if (teacher == null)
            throw new Exception("Teacher does not exist");
        
        patch.ApplyTo(teacher);
        await Context.SaveChangesAsync();
        return teacher;
    }

    public async Task<bool> RemoveTeacherAsync(int teacherId)
    {
        var teacher = await Context.Teachers
            .SingleOrDefaultAsync(q => q.TeacherId == teacherId);

        if (teacher == null)
        {
            // throw new Exception("Teacher does not exist");
            return false;
        }

        Context.Teachers.Remove(teacher);
        
        await Context.Classes.Where(q => q.TeacherId == teacherId).ForEachAsync(q => q.TeacherId = null);
        await Context.SaveChangesAsync();
        return true;
    }
}