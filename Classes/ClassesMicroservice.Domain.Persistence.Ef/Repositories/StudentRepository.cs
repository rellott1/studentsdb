﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Students;
using ClassesMicroservice.Dto.Json.Students;
using Microsoft.EntityFrameworkCore;

namespace ClassesMicroservice.Domain.Persistence.Ef.Repositories;

public class StudentRepository : EfRepository, IStudentRepository
{
    public StudentRepository(IClassesContext context) : base(context)
    {
    }

    public async Task<List<Student>> GetStudentsAsync()
    {
        return await Context.Students.ToListAsync();
    }

    public async Task<Student> GetStudentAsync(int studentId)
    {
        return await Context.Students.FirstOrDefaultAsync(q => q.StudentId == studentId);
    }

    public async Task<Student> CreateStudentAsync(CreateStudentDto createStudentDto)
    {
        if (createStudentDto.UserId.HasValue)
        {
            var user = await Context.Users
                .FirstOrDefaultAsync(q => q.UserId == createStudentDto.UserId.Value);

            if (user == null)
                return null;
        }
        
        var studentEntity = new Student
        {
            FullName = createStudentDto.FullName,
            GroupId = createStudentDto.GroupId,
            UserId = createStudentDto.UserId.Value
        };
        await Context.Students.AddAsync(studentEntity);
        await Context.SaveChangesAsync();
        return studentEntity;
    }

    public async Task<Student> UpdateStudentAsync(int studentId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/userId" && patch.Operations[0].op == "replace")
        {
            var user = await Context.Users
                .FirstOrDefaultAsync(q => q.UserId == (int) patch.Operations[0].value);

            if (user == null)
                return null;
        }
        
        var student = await Context.Students
            .SingleOrDefaultAsync(q => q.StudentId == studentId);

        if (student == null)
            throw new Exception("Student does not exist");
        
        patch.ApplyTo(student);
        await Context.SaveChangesAsync();
        return student;
    }

    public async Task<bool> RemoveStudentAsync(int studentId)
    {
        var student = await Context.Students
            .SingleOrDefaultAsync(q => q.StudentId == studentId);

        if (student == null)
        {
            // throw new Exception("Student does not exist");
            return false;
        }

        Context.Students.Remove(student);
        await Context.SaveChangesAsync();
        return true;
    }
    
    public async Task<List<Student>> GetAllStudentsFromGroupAsync(int groupId)
    {
        return await Context.Students.Where(q => q.GroupId == groupId).ToListAsync();
    }
}