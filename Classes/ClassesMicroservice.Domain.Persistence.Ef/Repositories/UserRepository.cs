﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Dto.Json.Users;
using Microsoft.EntityFrameworkCore;

namespace ClassesMicroservice.Domain.Persistence.Ef.Repositories;

public class UserRepository : EfRepository, IUserRepository
{
    public UserRepository(IClassesContext context) : base(context)
    {
    }

    public async Task<List<User>> GetUsersAsync()
    {
        return await Context.Users.ToListAsync();
    }

    public async Task<User> GetUserAsync(int userId)
    {
        return await Context.Users.FirstOrDefaultAsync(q => q.UserId == userId);
    }

    public async Task<User> CreateUserAsync(CreateUserDto createUserDto)
    {
        if (!Enum.IsDefined(typeof(Role), (byte) createUserDto.Role))
        {
            return null;
        }
        
        var userEntity = new User
        {
            Login = createUserDto.Login,
            Password = createUserDto.Password,
            Role = (Role) createUserDto.Role
        };
        await Context.Users.AddAsync(userEntity);
        await Context.SaveChangesAsync();
        return userEntity;
    }

    public async Task<User> UpdateUserAsync(int userId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/role" && patch.Operations[0].op == "replace")
        {
            if (!Enum.IsDefined(typeof(Role), Convert.ToByte(patch.Operations[0].value)))
            {
                return null;
            }
        }
        
        var user = await Context.Users
            .SingleOrDefaultAsync(q => q.UserId == userId);

        if (user == null)
            throw new Exception("User does not exist");
        
        patch.ApplyTo(user);
        await Context.SaveChangesAsync();
        return user;
    }

    public async Task<bool> RemoveUserAsync(int userId)
    {
        var user = await Context.Users
            .SingleOrDefaultAsync(q => q.UserId == userId);

        if (user == null)
        {
            // throw new Exception("User does not exist");
            return false;
        }

        Context.Users.Remove(user);

        await Context.Students.Where(q => q.UserId == userId).ForEachAsync(q => q.UserId = null);
        await Context.Teachers.Where(q => q.UserId == userId).ForEachAsync(q => q.UserId = null);
        await Context.SaveChangesAsync();
        return true;
    }
}