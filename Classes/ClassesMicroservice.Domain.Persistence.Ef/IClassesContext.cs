﻿using ClassesMicroservice.Domain.Users;
using ClassesMicroservice.Domain.Students;
using ClassesMicroservice.Domain.Teachers;
using ClassesMicroservice.Domain.Classes;
using Microsoft.EntityFrameworkCore;

namespace ClassesMicroservice.Domain.Persistence.Ef;

public interface IClassesContext : IDisposable
{
    DbSet<User> Users { get; set; }
    DbSet<Student> Students { get; set; }
    DbSet<Teacher> Teachers { get; set; }
    DbSet<Class> Classes { get; set; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}