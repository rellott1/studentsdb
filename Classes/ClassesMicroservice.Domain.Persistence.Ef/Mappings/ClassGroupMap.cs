﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ClassesMicroservice.Domain.Classes;

namespace ClassesMicroservice.Domain.Persistence.Ef.Mappings;

public class ClassGroupMap : IEntityTypeConfiguration<ClassGroup>
{
    public void Configure(EntityTypeBuilder<ClassGroup> builder)
    {
        builder.HasKey(e => e.Id);

        builder.HasOne(d => d.Class)
            .WithMany(p => p.ClassGroups)
            .HasForeignKey(d => d.ClassId)
            .HasConstraintName("FK_T_ClassGroup_T_Class");

        builder.ToTable("T_ClassGroups");
    }
}