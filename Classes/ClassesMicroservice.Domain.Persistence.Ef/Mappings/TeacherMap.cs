﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ClassesMicroservice.Domain.Teachers;

namespace ClassesMicroservice.Domain.Persistence.Ef.Mappings;

public class TeacherMap : IEntityTypeConfiguration<Teacher>
{
    public void Configure(EntityTypeBuilder<Teacher> builder)
    {
        builder.HasKey(e => e.TeacherId);
        builder.Property(e => e.FullName)
            .HasMaxLength(255);
        builder.Property(e => e.AcademicDegree)
            .HasMaxLength(255);

        builder.HasOne(d => d.User)
            .WithMany(p => p.Teachers)
            .HasForeignKey(d => d.UserId)
            .HasConstraintName("FK_T_Teacher_T_User")
            .OnDelete(DeleteBehavior.SetNull);
        
        builder.ToTable("T_Teachers");
    }
}