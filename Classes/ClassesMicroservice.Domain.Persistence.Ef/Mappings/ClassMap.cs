﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ClassesMicroservice.Domain.Classes;

namespace ClassesMicroservice.Domain.Persistence.Ef.Mappings;

public class ClassMap : IEntityTypeConfiguration<Class>
{
    public void Configure(EntityTypeBuilder<Class> builder)
    {
        builder.HasKey(e => e.ClassId);
        
        builder.Property(e => e.Classroom)
            .HasMaxLength(255);
        
        builder.HasOne(d => d.Teacher)
            .WithMany(p => p.Classes)
            .HasForeignKey(d => d.TeacherId)
            .HasConstraintName("FK_T_Class_T_Teacher")
            .OnDelete(DeleteBehavior.SetNull);
        
        builder.ToTable("T_Classes");
    }
}