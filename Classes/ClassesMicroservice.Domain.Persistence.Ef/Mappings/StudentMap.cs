﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ClassesMicroservice.Domain.Students;

namespace ClassesMicroservice.Domain.Persistence.Ef.Mappings;

public class StudentMap : IEntityTypeConfiguration<Student>
{
    public void Configure(EntityTypeBuilder<Student> builder)
    {
        builder.HasKey(e => e.StudentId);
        builder.Property(e => e.FullName)
            .HasMaxLength(255);

        builder.HasOne(d => d.User)
            .WithMany(p => p.Students)
            .HasForeignKey(d => d.UserId)
            .HasConstraintName("FK_T_Student_T_User")
            .OnDelete(DeleteBehavior.SetNull);

        builder.ToTable("T_Students");
    }
}