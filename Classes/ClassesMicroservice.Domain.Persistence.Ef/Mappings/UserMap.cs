﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ClassesMicroservice.Domain.Users;

namespace ClassesMicroservice.Domain.Persistence.Ef.Mappings;

public class UserMap : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasKey(e => e.UserId);
        builder.Property(e => e.Login)
            .HasMaxLength(255);
        builder.Property(e => e.Password)
            .HasMaxLength(255);

        builder.ToTable("T_Users");
    }
}