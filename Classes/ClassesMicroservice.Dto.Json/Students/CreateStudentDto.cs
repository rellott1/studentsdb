﻿namespace ClassesMicroservice.Dto.Json.Students;

public class CreateStudentDto
{
    public string FullName { get; set; }
    public int GroupId { get; set; }
    public int? UserId { get; set; }
}