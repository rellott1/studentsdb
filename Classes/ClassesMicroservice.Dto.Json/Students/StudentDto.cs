﻿namespace ClassesMicroservice.Dto.Json.Students;

public class StudentDto : CreateStudentDto
{
    public int Id { get; set; }
}