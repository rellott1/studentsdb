﻿namespace ClassesMicroservice.Dto.Json.Classes;

public class ClassDto : CreateClassDto
{
    public int Id { get; set; }
}