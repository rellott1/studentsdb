﻿namespace ClassesMicroservice.Dto.Json.Classes;

public class CreateClassDto
{
    public bool IsRemote { get; set; }
    public string Classroom { get; set; }
    public DateTime Date { get; set; }
    public int LessonType { get; set; }
    public int WeekNumber { get; set; }
    public int StudyHour { get; set; }
    public int SubjectId { get; set; }
    public int? TeacherId { get; set; }
}