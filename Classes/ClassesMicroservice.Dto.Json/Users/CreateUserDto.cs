﻿namespace ClassesMicroservice.Dto.Json.Users;

public class CreateUserDto
{
    public string Login { get; set; }
    public string Password { get; set; }
    public int Role { get; set; }
}