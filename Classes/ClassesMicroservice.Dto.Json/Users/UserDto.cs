﻿namespace ClassesMicroservice.Dto.Json.Users;

public class UserDto : CreateUserDto
{
    public int Id { get; set; }
}