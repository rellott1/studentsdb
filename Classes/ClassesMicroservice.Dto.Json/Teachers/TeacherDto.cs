﻿namespace ClassesMicroservice.Dto.Json.Teachers;

public class TeacherDto : CreateTeacherDto
{
    public int Id { get; set; }
}