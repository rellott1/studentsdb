﻿namespace ClassesMicroservice.Dto.Json.Teachers;

public class CreateTeacherDto
{
    public string FullName { get; set; }
    public string AcademicDegree { get; set; }
    public int DepartmentId { get; set; }
    public int? UserId { get; set; }
}