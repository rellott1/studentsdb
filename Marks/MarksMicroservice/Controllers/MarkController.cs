﻿using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Dto.Json.Marks;
using Microsoft.AspNetCore.JsonPatch;

namespace MarksMicroservice.Controllers;

public class MarkController
{
    private readonly IMarkRepository _markRepository;

    public MarkController(IMarkRepository markRepository)
    {
        _markRepository = markRepository;
    }

    public async Task<List<Mark>> GetMarksAsync()
    {
        return await _markRepository.GetMarksAsync();
    }

    public async Task<Mark> GetMarkAsync(string markId)
    {
        return await _markRepository.GetMarkAsync(markId);
    }

    public async Task<Mark> CreateMarkAsync(CreateMarkDto createMarkDto)
    {
        return await _markRepository.CreateMarkAsync(createMarkDto);
    }

    public async Task<Mark> UpdateMarkAsync(string markId, JsonPatchDocument patch)
    {
        return await _markRepository.UpdateMarkAsync(markId, patch);
    }

    public async Task<bool> RemoveMarkAsync(string markId)
    {
        return await _markRepository.RemoveMarkAsync(markId);
    }

    public async Task<double> GetPointsMean()
    {
        return await _markRepository.GetPointsMean();
    }
}