﻿using MarksMicroservice.Domain.StudentReviews;
using MarksMicroservice.Dto.Json.StudentReviews;
using Microsoft.AspNetCore.JsonPatch;

namespace MarksMicroservice.Controllers;

public class StudentReviewController
{
    private readonly IStudentReviewRepository _studentReviewRepository;

    public StudentReviewController(IStudentReviewRepository studentReviewRepository)
    {
        _studentReviewRepository = studentReviewRepository;
    }

    public async Task<List<StudentReview>> GetStudentReviewsAsync()
    {
        return await _studentReviewRepository.GetStudentReviewsAsync();
    }

    public async Task<StudentReview> GetStudentReviewAsync(string studentReviewId)
    {
        return await _studentReviewRepository.GetStudentReviewAsync(studentReviewId);
    }

    public async Task<StudentReview> CreateStudentReviewAsync(CreateStudentReviewDto createStudentReviewDto)
    {
        return await _studentReviewRepository.CreateStudentReviewAsync(createStudentReviewDto);
    }

    public async Task<StudentReview> UpdateStudentReviewAsync(string studentReviewId, JsonPatchDocument patch)
    {
        return await _studentReviewRepository.UpdateStudentReviewAsync(studentReviewId, patch);
    }

    public async Task<bool> RemoveStudentReviewAsync(string studentReviewId)
    {
        return await _studentReviewRepository.RemoveStudentReviewAsync(studentReviewId);
    }

    public async Task<List<StudentReview>> GetAllPositiveStudentReviewsAsync()
    {
        return await _studentReviewRepository.GetAllPositiveStudentReviewsAsync();
    }
}