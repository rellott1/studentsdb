﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MarksMicroservice.Domain.Marks;

namespace MarksMicroservice.Domain.StudentReviews;

public class StudentReview
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string StudentReviewId { get; set; }
    public bool Useful { get; set; }
    public bool Easy { get; set; }
    public bool Like { get; set; }
    public string MarkId { get; set; }
}