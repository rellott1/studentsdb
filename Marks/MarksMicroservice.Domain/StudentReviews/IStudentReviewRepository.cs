﻿using MarksMicroservice.Dto.Json.StudentReviews;
using Microsoft.AspNetCore.JsonPatch;

namespace MarksMicroservice.Domain.StudentReviews;

public interface IStudentReviewRepository
{
    Task<List<StudentReview>> GetStudentReviewsAsync();
    Task<StudentReview> GetStudentReviewAsync(string studentReviewId);
    Task<StudentReview> CreateStudentReviewAsync(CreateStudentReviewDto createStudentReviewDto);
    Task<StudentReview> UpdateStudentReviewAsync(string studentReviewId, JsonPatchDocument patch);
    Task<bool> RemoveStudentReviewAsync(string studentReviewId);
    Task<List<StudentReview>> GetAllPositiveStudentReviewsAsync();
}