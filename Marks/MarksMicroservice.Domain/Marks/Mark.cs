﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MarksMicroservice.Domain.StudentReviews;

namespace MarksMicroservice.Domain.Marks;

public class Mark
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string MarkId { get; set; }
    public string WorkName { get; set; }
    public string WorkDescription { get; set; }
    public string WorkComments { get; set; }
    public int Points { get; set; }
    public int StudentId { get; set; }
    public bool StudentPresence { get; set; }
    public DateTime Date { get; set; }
    public string? StudentReviewId { get; set; }
}