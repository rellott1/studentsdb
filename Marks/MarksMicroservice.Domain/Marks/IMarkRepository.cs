﻿using MarksMicroservice.Dto.Json.Marks;
using Microsoft.AspNetCore.JsonPatch;

namespace MarksMicroservice.Domain.Marks;

public interface IMarkRepository
{
    Task<List<Mark>> GetMarksAsync();
    Task<Mark> GetMarkAsync(string markId);
    Task<Mark> CreateMarkAsync(CreateMarkDto createMarkDto);
    Task<Mark> UpdateMarkAsync(string markId, JsonPatchDocument patch);
    Task<bool> RemoveMarkAsync(string markId);
    Task<double> GetPointsMean();
}