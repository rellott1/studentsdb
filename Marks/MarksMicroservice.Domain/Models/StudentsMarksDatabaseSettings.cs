﻿namespace MarksMicroservice.Domain.Models;

public class StudentsMarksDatabaseSettings
{
    public string ConnectionString { get; set; }
    public string DatabaseName { get; set; }
    public string MarksCollectionName { get; set; }
    public string StudentReviewsCollectionName { get; set; }
}