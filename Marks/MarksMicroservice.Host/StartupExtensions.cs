﻿using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using MarksMicroservice.Controllers;
using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Domain.StudentReviews;
using MarksMicroservice.Domain.Persistence.EfLike;
using MarksMicroservice.Domain.Persistence.EfLike.Repositories;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: InternalsVisibleTo("MarksMicroservice.Host")]
namespace MarksMicroservice.Host;

public static class StartupExtensions
{
    public static IServiceCollection RegisterApplicationControllers(this IServiceCollection @this)
    {
        @this.AddScoped<MarkController, MarkController>();
        @this.AddScoped<StudentReviewController, StudentReviewController>();
        return @this;
    }
    
    public static IServiceCollection RegisterServices(this IServiceCollection @this)
    {
        return @this;
    }
    
    // public static IServiceCollection RegisterDatabase(this IServiceCollection @this, string connectionString)
    // {
    //     @this.AddDbContext<MarksContext>(options =>
    //         options.UseSqlServer(connectionString,
    //             builder => builder
    //                 .EnableRetryOnFailure()
    //                 // .MigrationsAssembly("MarksMicroservice.Domain.Persistence.EfLike")
    //             ));
    //
    //     return @this;
    // }

    public static IServiceCollection RegisterRepositories(this IServiceCollection @this)
    {
        @this.AddScoped<IMarksContext, MarksContext>();

        @this.AddScoped<IMarkRepository, MarkRepository>();
        @this.AddScoped<IStudentReviewRepository, StudentReviewRepository>();
        return @this;
    }

    public static IServiceCollection RegisterDomainEvents(this IServiceCollection @this)
    {
        return @this;
    }
}