﻿using MarksMicroservice.Domain.StudentReviews;
using MarksMicroservice.Dto.Json.StudentReviews;

namespace MarksMicroservice.Host.Extensions.StudentReviews;

public static class StudentReviewExtensions
{
    public static StudentReviewDto ToJsonDto(this StudentReview studentReviewEntity)
    {
        return new StudentReviewDto
        {
            Id = studentReviewEntity.StudentReviewId,
            Useful = studentReviewEntity.Useful,
            Easy = studentReviewEntity.Easy,
            Like = studentReviewEntity.Like,
            MarkId = studentReviewEntity.MarkId
        };
    }

    public static StudentReviewDto[] ToJsonDto(this IEnumerable<StudentReview> studentReviews)
    {
        return studentReviews.Select(at => at.ToJsonDto()).ToArray();
    }
}