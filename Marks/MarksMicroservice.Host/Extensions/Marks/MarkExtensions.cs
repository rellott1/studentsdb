﻿using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Dto.Json.Marks;

namespace MarksMicroservice.Host.Extensions.Marks;

public static class MarkExtensions
{
    public static MarkDto ToJsonDto(this Mark markEntity)
    {
        return new MarkDto
        {
            Id = markEntity.MarkId,
            WorkName = markEntity.WorkName,
            WorkDescription = markEntity.WorkDescription,
            WorkComments = markEntity.WorkComments,
            Points = markEntity.Points,
            StudentId = markEntity.StudentId,
            StudentPresence = markEntity.StudentPresence,
            Date = markEntity.Date,
            StudentReviewId = markEntity.StudentReviewId
        };
    }

    public static MarkDto[] ToJsonDto(this IEnumerable<Mark> marks)
    {
        return marks.Select(at => at.ToJsonDto()).ToArray();
    }
}