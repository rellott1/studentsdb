﻿using System.Diagnostics;
using System.Net;
using System.Net.Http.Headers;
using MarksMicroservice.Controllers;
using MarksMicroservice.Dto.Json.Marks;
using MarksMicroservice.Dto.Json.Students;
using MarksMicroservice.Host.Extensions.Marks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

namespace MarksMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/marks")]
[Produces("application/json")]
public class MarkApiController : ControllerBase
{
    private readonly MarkController _markController;

    public MarkApiController(MarkController markController)
    {
        _markController = markController;
    }

    private async Task<bool> GetStudentPrecense(string studentId)
    {
        var url = $"https://localhost:7064/v1/students?studentId={studentId}";
        var httpRequest = (HttpWebRequest) WebRequest.Create(url);
        httpRequest.Headers["X-SM-Api-Key"] = "777";
        var httpResponse = (HttpWebResponse) await httpRequest.GetResponseAsync();

        return httpResponse.StatusCode != HttpStatusCode.NoContent;
    }

    [ProducesResponseType(typeof(MarkDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<MarkDto[]>> GetMarks()
    {
        var result = await _markController.GetMarksAsync();
        
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(MarkDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<MarkDto>> GetMark(string? markId)
    {
        var result = markId != null
            ? await _markController.GetMarkAsync(markId)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(typeof(MarkDto), StatusCodes.Status200OK)]
    [HttpGet("pointsMean")]
    public async Task<double> GetPointsMean()
    {
        return await _markController.GetPointsMean();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(MarkDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<MarkDto>> CreateMark([FromBody] CreateMarkDto createMarkDto)
    {
        bool exists = await GetStudentPrecense(createMarkDto.StudentId.ToString());
        
        if (!exists)
        {
            return BadRequest();
        }

        var result = await _markController.CreateMarkAsync(createMarkDto);
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{markId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<MarkDto>> UpdateMarkAsync(string markId, [FromBody]JsonPatchDocument patch)
    {
        if (patch.Operations[0].op == "replace" && patch.Operations[0].path == "/studentId")
        {
            bool exists = await GetStudentPrecense(patch.Operations[0].value.ToString());

            if (!exists)
            {
                return BadRequest();
            }
        }

        var request = await _markController.UpdateMarkAsync(markId, patch);
        return request.ToJsonDto();
    }
    
    [HttpDelete("{markId}")]
    public async Task<ActionResult<bool>> DeleteMark(string? markId)
    {
        var result = markId != null
            ? await _markController.RemoveMarkAsync(markId)
            : false;

        return result;
    }
}