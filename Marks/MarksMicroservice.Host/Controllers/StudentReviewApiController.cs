﻿using MarksMicroservice.Controllers;
using MarksMicroservice.Dto.Json.StudentReviews;
using MarksMicroservice.Host.Extensions.StudentReviews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarksMicroservice.Host.Controllers;

[Authorize]
[ApiController]
[Route("v1/studentReviews")]
[Produces("application/json")]
public class StudentReviewApiController : ControllerBase
{
    private readonly StudentReviewController _studentReviewController;

    public StudentReviewApiController(StudentReviewController studentReviewController)
    {
        _studentReviewController = studentReviewController;
    }

    [ProducesResponseType(typeof(StudentReviewDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("all")]
    public async Task<ActionResult<StudentReviewDto[]>> GetStudentReviews()
    {
        var result = await _studentReviewController.GetStudentReviewsAsync();
        
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(StudentReviewDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet("allPositive")]
    public async Task<ActionResult<StudentReviewDto[]>> GetAllPositiveStudentReviews()
    {
        var result = await _studentReviewController.GetAllPositiveStudentReviewsAsync();
        
        if (result != null)
            return result.ToJsonDto();

        return NoContent();
    }
    
    [ProducesResponseType(typeof(StudentReviewDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpGet]
    public async Task<ActionResult<StudentReviewDto>> GetStudentReview(string? studentReviewId)
    {
        var result = studentReviewId != null
            ? await _studentReviewController.GetStudentReviewAsync(studentReviewId)
            : null;
        
        if (result != null)
            return result.ToJsonDto();
            
        return NoContent();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(StudentReviewDto), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult<StudentReviewDto>> CreateStudentReview([FromBody] CreateStudentReviewDto createStudentReviewDto)
    {
        var result = await _studentReviewController.CreateStudentReviewAsync(createStudentReviewDto);
        
        if (result != null)
            return result.ToJsonDto();
            
        return BadRequest();
    }
    
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [HttpPatch("{studentReviewId}")]
    [Consumes("application/json-patch+json")]
    public async Task<ActionResult<StudentReviewDto>> UpdateStudentReviewAsync(string studentReviewId, [FromBody]JsonPatchDocument patch)
    {
        var request = await _studentReviewController.UpdateStudentReviewAsync(studentReviewId, patch);
        if (request != null)
            return request.ToJsonDto();
        
        return BadRequest();
}
    
    [HttpDelete("{studentReviewId}")]
    public async Task<ActionResult<bool>> DeleteStudentReview(string? studentReviewId)
    {
        var result = studentReviewId != null
            ? await _studentReviewController.RemoveStudentReviewAsync(studentReviewId)
            : false;

        return result;
    }
}