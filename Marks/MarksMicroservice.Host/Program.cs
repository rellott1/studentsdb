using MarksMicroservice.Host;
using MarksMicroservice.Host.Clients;
using MarksMicroservice.Host.Security;
using MarksMicroservice.Domain.Models;
using MarksMicroservice.Domain.Persistence.EfLike;
using MarksMicroservice.Host.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
builder.Services
    .AddAuthentication(ApiKeyAuthenticationDefaults.AuthenticationScheme)
    .AddApiKey();

builder.Services
    .AddAuthorization()
    .AddMemoryCache()
    .AddControllers()
    .AddNewtonsoftJson();

builder.Services
    .AddHttpClient<MarksClient>()
    .ConfigureHttpClient((provider, httpClient) =>
    {
        httpClient.BaseAddress = new Uri("http://192.168.250.65");
    });

builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Version = "v1",
            Title = "Marks API",
            Description = "API for working with the marks",
            Contact = new OpenApiContact
            {
                Name = "Alexander Rublev",
                Email = "alexander.rublev@aug-e.io"
            },
        });
        c.AddSecurityDefinition("ApiKey", new OpenApiSecurityScheme()
        {
            Type = SecuritySchemeType.ApiKey,
            In = ParameterLocation.Header,
            Name = builder.Configuration["SecurityOptions:AuthorizationHeader"],
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "ApiKey" }
                },
                new string[] { }
            }
        });
    }
);

// var databaseConnectionString = builder.Configuration["ConnectionStrings:MarksDb"];

builder.Services
    .AddHttpContextAccessor()
    // .RegisterDatabase(databaseConnectionString)
    .RegisterRepositories()
    .RegisterApplicationControllers();

builder.Services.Configure<StudentsMarksDatabaseSettings>(
    builder.Configuration.GetSection("StudentsMarksDatabase"));

builder.Services.AddSingleton<MarksContext>();



var app = builder.Build();

app
    .UseHttpsRedirection()
    .UseRouting()
    .UseAuthentication()
    .UseAuthorization()
    .UseEndpoints(endpoints => { endpoints.MapControllers(); })
    .UseSwagger()
    .UseSwaggerUI();

app.Run();