﻿namespace MarksMicroservice.Dto.Json.StudentReviews;

public class CreateStudentReviewDto
{
    public bool Useful { get; set; }
    public bool Easy { get; set; }
    public bool Like { get; set; }
    public string MarkId { get; set; }
}