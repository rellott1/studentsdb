﻿namespace MarksMicroservice.Dto.Json.StudentReviews;

public class StudentReviewDto : CreateStudentReviewDto
{
    public string Id { get; set; }
}