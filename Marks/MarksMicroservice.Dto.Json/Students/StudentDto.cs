﻿namespace MarksMicroservice.Dto.Json.Students;

public class StudentDto
{
    public int id { get; set; }
    public string fullName { get; set; }
    public int groupId { get; set; }
    public int userId { get; set; }
}