﻿namespace MarksMicroservice.Dto.Json.Marks;

public class CreateMarkDto
{
    public string WorkName { get; set; }
    public string WorkDescription { get; set; }
    public string WorkComments { get; set; }
    public int Points { get; set; }
    public int StudentId { get; set; }
    public bool StudentPresence { get; set; }
    public DateTime Date { get; set; }
    public string? StudentReviewId { get; set; }
}