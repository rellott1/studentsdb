﻿namespace MarksMicroservice.Dto.Json.Marks;

public class MarkDto : CreateMarkDto
{
    public string Id { get; set; }
}