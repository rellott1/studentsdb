﻿namespace MarksMicroservice.Domain.Persistence.EfLike.Repositories;

public class EfLikeRepository
{
    protected readonly IMarksContext Context;

    protected EfLikeRepository(IMarksContext context)
    {
        Context = context;
    }
}