﻿using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Domain.StudentReviews;
using MarksMicroservice.Dto.Json.Marks;
using Microsoft.AspNetCore.JsonPatch;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MarksMicroservice.Domain.Persistence.EfLike.Repositories;

public class MarkRepository : EfLikeRepository, IMarkRepository
{
    public MarkRepository(IMarksContext context) : base(context)
    {
    }
    
    public async Task<List<Mark>> GetMarksAsync()
    {
        return await Context.Marks.Find(_ => true).ToListAsync();
    }

    public async Task<Mark> GetMarkAsync(string markId)
    {
        return await Context.Marks.Find(q => q.MarkId == markId).FirstOrDefaultAsync();
    }

    public async Task<Mark> CreateMarkAsync(CreateMarkDto createMarkDto)
    {
        int count = (int) await Context.StudentReviews
            .Find(q => q.StudentReviewId == createMarkDto.StudentReviewId)
            .CountDocumentsAsync();

        if (count == 0)
            return null;

        var markEntity = new Mark
        {
            WorkName = createMarkDto.WorkName,
            WorkDescription = createMarkDto.WorkDescription,
            WorkComments = createMarkDto.WorkComments,
            Points = createMarkDto.Points,
            StudentId = createMarkDto.StudentId,
            StudentPresence = createMarkDto.StudentPresence,
            Date = createMarkDto.Date,
            StudentReviewId = createMarkDto.StudentReviewId
        };
        
        await Context.Marks.InsertOneAsync(markEntity);
        return markEntity;
    }

    public async Task<Mark> UpdateMarkAsync(string markId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/studentReviewId" && patch.Operations[0].op == "replace")
        {
            var studentReview = await Context.StudentReviews
                .Find(q => q.StudentReviewId == patch.Operations[0].value)
                .FirstOrDefaultAsync();

            if (studentReview == null)
                return null;
        }
        
        var mark = await Context.Marks
            .Find(q => q.MarkId == markId).FirstOrDefaultAsync();

        if (mark == null)
            throw new Exception("Mark does not exist");
        
        patch.ApplyTo(mark);
        await Context.Marks.ReplaceOneAsync(q => q.MarkId == markId, mark);
        return mark;
    }

    public async Task<bool> RemoveMarkAsync(string markId)
    {
        var mark = await Context.Marks
            .Find(q => q.MarkId == markId).FirstOrDefaultAsync();

        if (mark == null)
        {
            // throw new Exception("Mark does not exist");
            return false;
        }

        await Context.Marks.DeleteOneAsync(q => q.MarkId == markId);
        var update = Builders<StudentReview>.Update.Set(q => q.MarkId, null);
        await Context.StudentReviews.UpdateOneAsync(q => q.MarkId == markId, update);
        return true;
    }

    public async Task<double> GetPointsMean()
    {
        var marks = await Context.Marks.Find(_ => true).ToListAsync();
        return marks.Average(q => q.Points);
    }
}