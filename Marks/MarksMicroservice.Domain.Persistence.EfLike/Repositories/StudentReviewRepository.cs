﻿using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Domain.StudentReviews;
using MarksMicroservice.Dto.Json.Marks;
using MarksMicroservice.Dto.Json.StudentReviews;
using Microsoft.AspNetCore.JsonPatch;
using MongoDB.Driver;

namespace MarksMicroservice.Domain.Persistence.EfLike.Repositories;

public class StudentReviewRepository : EfLikeRepository, IStudentReviewRepository
{
    public StudentReviewRepository(IMarksContext context) : base(context)
    {
    }
    
    public async Task<List<StudentReview>> GetStudentReviewsAsync()
    {
        return await Context.StudentReviews.Find(_ => true).ToListAsync();
    }

    public async Task<StudentReview> GetStudentReviewAsync(string studentReviewId)
    {
        return await Context.StudentReviews.Find(q => q.StudentReviewId == studentReviewId).FirstOrDefaultAsync();
    }

    public async Task<StudentReview> CreateStudentReviewAsync(CreateStudentReviewDto createStudentReviewDto)
    {
        int count = (int) await Context.Marks
            .Find(q => q.MarkId == createStudentReviewDto.MarkId)
            .CountDocumentsAsync();

        if (count == 0)
            return null;

        var studentReviewEntity = new StudentReview
        {
            Useful = createStudentReviewDto.Useful,
            Easy = createStudentReviewDto.Easy,
            Like = createStudentReviewDto.Like,
            MarkId = createStudentReviewDto.MarkId
        };
        
        await Context.StudentReviews.InsertOneAsync(studentReviewEntity);
        return studentReviewEntity;
    }

    public async Task<StudentReview> UpdateStudentReviewAsync(string studentReviewId, JsonPatchDocument patch)
    {
        if (patch.Operations[0].path == "/markId" && patch.Operations[0].op == "replace")
        {
            var mark = await Context.Marks
                .Find(q => q.MarkId == patch.Operations[0].value)
                .FirstOrDefaultAsync();

            if (mark == null)
                return null;
        }
        
        var studentReview = await Context.StudentReviews
            .Find(q => q.StudentReviewId == studentReviewId).FirstOrDefaultAsync();

        if (studentReview == null)
            throw new Exception("Student review does not exist");

        patch.ApplyTo(studentReview);
        await Context.StudentReviews.ReplaceOneAsync(q => q.StudentReviewId == studentReviewId, studentReview);
        return studentReview;
    }

    public async Task<bool> RemoveStudentReviewAsync(string studentReviewId)
    {
        var studentReview = await Context.StudentReviews
            .Find(q => q.StudentReviewId == studentReviewId).FirstOrDefaultAsync();

        if (studentReview == null)
        {
            // throw new Exception("Student review does not exist");
            return false;
        }

        await Context.StudentReviews.DeleteOneAsync(q => q.StudentReviewId == studentReviewId);
        var update = Builders<Mark>.Update.Set(q => q.StudentReviewId, null);
        await Context.Marks.UpdateOneAsync(q => q.StudentReviewId == studentReviewId, update);
        return true;
    }
    
    public async Task<List<StudentReview>> GetAllPositiveStudentReviewsAsync()
    {
        return await Context.StudentReviews
            .Find(q => q.Useful && q.Easy && q.Like)
            .ToListAsync();
    }
}