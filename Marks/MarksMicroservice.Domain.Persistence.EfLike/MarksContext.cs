﻿using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Domain.StudentReviews;
using MarksMicroservice.Domain.Models;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;

namespace MarksMicroservice.Domain.Persistence.EfLike;

public class MarksContext : IMarksContext
{
    public IMongoDatabase database { get; set; }
    private string MarksCollectionName { get; set; }
    private string StudentReviewsCollectionName { get; set; }
    
    public MarksContext(IOptions<StudentsMarksDatabaseSettings> studentsMarksDatabaseSettings)
    {
        MongoClient client = new MongoClient(studentsMarksDatabaseSettings.Value.ConnectionString);
        database = client.GetDatabase(studentsMarksDatabaseSettings.Value.DatabaseName);
        MarksCollectionName = studentsMarksDatabaseSettings.Value.MarksCollectionName;
        StudentReviewsCollectionName = studentsMarksDatabaseSettings.Value.StudentReviewsCollectionName;
    }
    
    public IMongoCollection<Mark> Marks => database.GetCollection<Mark>(MarksCollectionName);
    
    public IMongoCollection<StudentReview> StudentReviews =>
        database.GetCollection<StudentReview>(StudentReviewsCollectionName);
}