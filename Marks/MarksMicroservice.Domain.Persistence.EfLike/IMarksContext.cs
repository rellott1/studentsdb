﻿using MongoDB.Driver;
using MarksMicroservice.Domain.Marks;
using MarksMicroservice.Domain.StudentReviews;

namespace MarksMicroservice.Domain.Persistence.EfLike;

public interface IMarksContext
{
    IMongoDatabase database { get; set; }
    
    IMongoCollection<Mark> Marks { get; }
    
    IMongoCollection<StudentReview> StudentReviews { get; }
}